# EC Image Bank

## Prerequisites

* ### NodeJS && npm
	* #### Mac
		1. Go to [nodejs.org](http://nodejs.org).
		2. Click **install** (to download the pkg).
		3. Open the pkg and run through the install process.
		4. That's it!
	* #### Ubuntu
        On the latest version of Ubuntu, you can simply:

            sudo apt-get install nodejs nodejs-dev npm

        On earlier versions, you might need to update your repository:

            sudo apt-get install python-software-properties
            sudo add-apt-repository ppa:chris-lea/node.js
            sudo apt-get update
            sudo apt-get install nodejs nodejs-dev npm

* ### Grunt-CLI

	    sudo npm install -g grunt-cli

## Installation & customization

    git clone git@bitbucket.org:ala_747/ec-image-bank.git New_project_name

or

	git clone https://USERNAME@bitbucket.org/ala_747/ec-image-bank.git New_project_name

Then,

	sudo npm install

Then, open `___vars.theme.php` and customize everything the project need, edit `___data.json.php`, put the images at `public/img/`, put the thumbnails at `public/thumbs/` and the Client logo at `public/logos/`.

If the product will use the Local Login Semifake System, rename `public/password.csv_RENAME` (which has one test user defined as "PosterMaker" with password "test") to `public/password.csv` (or directly put a file called `password.csv` into `public/` with all the users in it following the format `username;password` one per line). Then, overwrite the images `public/login-image.jpg` (the background image) and `public/logos.jpg` (the image below the login form) with the product ones.

Then,

    grunt

Then, open **FTP** to the webserver and browse to `/var/www/sec.ec-europe.com/image-bank/`, create a new directory and upload all `New_project_name/dist` contents to it (make sure to have 'Show hidden files' checked) and change `your_uploaded_directory/download/` directory permissions to `0777` into the webserver.

Then... point a browser to `https://sec.ec-europe.com/image-bank/your_uploaded_directory`, look if everything is fine and relax. That's all.