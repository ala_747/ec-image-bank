module.exports = function(grunt) {
    'use strict';

    // LOAD MODULES, MAGICALLY
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        // PACKAGE INFO
        pkg: grunt.file.readJSON('package.json'),

        // FOLDERS
        SOURCE : {
            JS      : 'sources/js',
            CSS     : 'sources/css',
            SVG     : 'sources/svg'
        },

        BUILDS : {
            JS      : 'public/js',
            CSS     : 'public/css',
            ICO     : 'public/icons'
        },

        concat: {
            options: {
                banner: '/*! \n<%= pkg.name %> <%= pkg.version %>, <%= pkg.author %>\n<%= pkg.homepage %>\n*/\n',
                separator: ';'
            },
            // head scripts
            oldie: {
                src: ['<%= SOURCE.JS %>/polyfills.js', '<%= SOURCE.JS %>/underscore.js', '<%= SOURCE.JS %>/jquery.1.js', '<%= SOURCE.JS %>/pubsub.js', '<%= SOURCE.JS %>/zgtc.stats.js', '<%= SOURCE.JS %>/zgtc.navigator.js'],
                dest: '<%= SOURCE.JS %>/_oldie.navigator.js'
            },
            goodie: {
                src: ['<%= SOURCE.JS %>/polyfills.js', '<%= SOURCE.JS %>/underscore.js', '<%= SOURCE.JS %>/jquery.js', '<%= SOURCE.JS %>/pubsub.js', '<%= SOURCE.JS %>/zgtc.stats.js', '<%= SOURCE.JS %>/zgtc.navigator.js'],
                dest: '<%= SOURCE.JS %>/_goodie.navigator.js'
            },
            // editor scripts
            oldie_editor: {
                // add excanvas and old fabric for old IE compat.
                src: ['<%= SOURCE.JS %>/rangeslider.js', '<%= SOURCE.JS %>/spectrum.js', '<%= SOURCE.JS %>/excanvas.js', '<%= SOURCE.JS %>/fabric.ie8.js', '<%= SOURCE.JS %>/fabric.arrow.js', '<%= SOURCE.JS %>/chronos.js', '<%= SOURCE.JS %>/zgtc.editor.js'],
                dest: '<%= SOURCE.JS %>/_oldie.editor.js'
            },
            goodie_editor: {
                src: ['<%= SOURCE.JS %>/rangeslider.js', '<%= SOURCE.JS %>/spectrum.js', '<%= SOURCE.JS %>/fabric.js', '<%= SOURCE.JS %>/fabric.arrow.js', '<%= SOURCE.JS %>/chronos.js', '<%= SOURCE.JS %>/zgtc.editor.js'],
                dest: '<%= SOURCE.JS %>/_goodie.editor.js'
            },
        },

        uglify: {
            options: {
                compress: {
                    drop_console: true
                },
                mangle: true,
                preserveComments: 'some'
            },
            head: {
                // only script in head:
                // scout decides if it has to load jquery v1/v2…
                files: {
                    '<%= BUILDS.JS %>/loader.min.js': ['<%= SOURCE.JS %>/zgtc.scout.js']
                }
            },
            oldie: {
                options: {
                    compress:  true,
                    mangle: false,
                    preserveComments: 'some'
                },
                files: {
                    '<%= BUILDS.JS %>/oldie.navigator.min.js': ['<%= concat.oldie.dest %>']
                }
            },
            goodie: {
                files: {
                    '<%= BUILDS.JS %>/goodie.navigator.min.js': ['<%= concat.goodie.dest %>']
                }
            },
            oldie_editor: {
                options: {
                    compress:  true,
                    mangle: false,
                    preserveComments: 'some'
                },
                files: {
                    '<%= BUILDS.JS %>/oldie.editor.min.js': ['<%= concat.oldie_editor.dest %>']
                }
            },
            goodie_editor: {
                files: {
                    '<%= BUILDS.JS %>/goodie.editor.min.js': ['<%= concat.goodie_editor.dest %>']
                }
            }
        },

        connect: {
            uses_defaults: {}
        },

        compass: {
            dist: {
                options: {
                    config: 'config.rb'
                }
            }
        },

        copy: {
            main: {
                files: [
                    {expand: true, src: ['*.php'], dest: 'dist/', filter: 'isFile'},

                    {expand: true, src: ['public/**'], dest: 'dist/'},
                    {expand: true, src: ['fonts/**'], dest: 'dist/'},

                    {expand: true, src: ['download/**'], dest: 'dist/', mode: true},
                    {expand: true, src: ['download/.htaccess'], dest: 'dist/'},
                ],
            },
        },

        watch: {
            main: {
                files: ['*.html', '*.php', 'Gruntfile.js', '<%= SOURCE.JS %>/*.js', '<%= BUILDS.JS %>/*.js', '<%= BUILDS.CSS %>/*.css'],
                tasks: 'default'
            },
            options: {
                livereload: true,
                interrupt: true,
                nospawn: true
            }
        }
    });

    // REGISTER TASKS
    grunt.registerTask('default', ['concat:oldie', 'concat:goodie', 'concat:oldie_editor', 'concat:goodie_editor', 'uglify:head', 'uglify:oldie', 'uglify:goodie', 'uglify:oldie_editor', 'uglify:goodie_editor', 'compass', 'copy']);
    grunt.registerTask('live', ['connect', 'watch:main']);

    // write file actions to terminal
    grunt.event.on('watch', function(action, filepath, target) {
        grunt.log.writeln(target + ': ' + filepath + ' has ' + action);
    });

};
