

/*!
------------------------------------------------------------------------------------------------------
                                                                                                 CACHE
 ----------------------------------------------------------------------------------------------------- */
ZGTC.CACHE = {
    init : function(){
        this.W        = $(window);
        this.HD       = $(document.getElementsByTagName("html")[0]);
        this.DOC      = $(document);
        this.BODY     = $(document.body);
        this.UA       = navigator.userAgent.toLowerCase();
        this.EA       = $("#editarea");
    },

    standarizeEvents : function(){
        this.CLICK    = ('ontouchstart' in window)?"touchend":"click";
        this.TAP      = ('ontouchstart' in window)?"touchstart":"click";
        this.TRANS    = (!!window.webkitURL)?"webkitTransitionEnd":"transitionend";
        // webkitTransitionEnd works for chrome, safari, opera
        // elegant solution at http://stackoverflow.com/a/18106816
        // does not trigger twice, not even once for webkit since sets transitionEnd instead of webkitTransitionEnd
        // !!window.MozApplicationEvent moz16+
    }
};


/*!
------------------------------------------------------------------------------------------------------
                                                                                          UTILS MODULE
 ----------------------------------------------------------------------------------------------------- */

ZGTC.Utils = {
    setOrientationBreakpoints : function(){
        var W   = ZGTC.CACHE.W,
            HD  = ZGTC.CACHE.HD,
            // we should be using parseFloat($("body").css("font-size")),
            // but CSS mediaqueries are triggered with 16px base even if set in EM…
            FS  = 16,
            w   = W.width(),
            h   = W.height(),
            EM  = w / FS,
            BP, OR;

        // orientation
        HD.removeClass("orientation-land orientation-vert bp-mobile bp-tablet");
        if(w>h) OR = "orientation-land";
        else OR = "orientation-vert";

        // breakpoints. must be sync'ed manually with CSS MQ
        if(EM > 0 && EM < 46.5) BP = "bp-mobile";
        if(EM >= 46.5) BP = "bp-tablet";

        // set
        HD.addClass(" "+OR+" "+BP+" ");

        // fix content width, we cannot use a table to scroll nor css calc
        if( ZGTC.Utils.isCry() ){
            $("#content").width( W.width() - $("#menu").width() - 2); // border
        }
    },

    isIE : function(){
        var UA      = ZGTC.CACHE.UA,
            msie    = UA.indexOf("msie") > -1,
            trident = UA.indexOf("trident") > -1;
        return msie || trident;
    },

    isCry : function(){
        var UA      = ZGTC.CACHE.UA,
            msie    = UA.indexOf("msie 8") > -1;
        return msie;
    },

    isSafari: function () {
        var UA = ZGTC.CACHE.UA,
            safari = UA.indexOf("safari") > -1,
            chrome = UA.indexOf("chrome") > -1;

        return !(chrome && safari);
    },

    getElmId : function( elm ){
        // utility that gets an objet and checks if it has ID, adds if needed and returns ID;
        var ID      = elm[0].id,
            seed    = parseInt(10000 * Math.random(), null);
        if(ID === "") {
            elm[0].id = "randomid-"+seed;
        }
        return elm[0].id;
    },

    toggleState : function(elem, a, b) {
        var E   = $(elem),
            isA = E.hasClass(a);

        if(isA){
            E.removeClass(a).addClass(b);
        }
        else {
            E.removeClass(b).addClass(a);
        }
    }
};


/*!
------------------------------------------------------------------------------------------------------
                                                                                  SEARCH/FILTER MODULE
 ----------------------------------------------------------------------------------------------------- */
ZGTC.Browse = {
    JSON    : null,
    books   : {},
    Books   : [],
    Chaps   : [],
    linx    : [],
    scored  : [],
    filtrd  : [],
    what    : [],
    timer0  : null,
    timer1  : null,
    tplmin  : null,
    tpl     : null,
    editable: 0,

    // ------------------------------------------------------------------------------------ INIT
    init : function(){
        // set context:
        var ctx = ZGTC.Browse;

        // get _ templates
        ctx.tplmin  = $("#thumb_template_minimal").html();
        ctx.tpl     = $("#thumb_template").html();

        // load JSON from server query
        //$.getJSON("json.php", function( json ) {
            // store data into persistent object
            ctx.JSON = J.items;
            // prepare form
            ctx.formable();
            // prepare filter links
            ctx.buildstructure();
        //});

        // pubsub: when editor js is ready, activate links
        $.subscribe("ready.editor", this.activateThumbLinks);
        $.subscribe("ready.editor", this.informClickable);

        // publish to stats
        $.publish( "canvas.stats", {"Category":"browse images", "Action":"init"} );
    },

    // ------------------------------------------------------------------------------------ OUTPUT PROCESS DURATION
    filterTimerStart : function(){
        // set context:
        var ctx = ZGTC.Browse;

        ctx.timer0 = Date.now();
        console.log("--- TIMER STARTS");
    },

    filterTimerEnd : function(){
        // set context:
        var ctx = ZGTC.Browse;

        ctx.timer1 = Date.now();
        console.log("--- TIMER ENDS", ctx.timer1 - ctx.timer0);
        ctx.filterTimerReset();
    },

    filterTimerReset : function(){
        // set context:
        var ctx = ZGTC.Browse;

        ctx.timer0 = ctx.timer1 = null;
    },

    // ------------------------------------------------------------------------------------ OPTION 1. SEARCH ENGINE
    formable : function(){
        // show form
        $("#search").show();
        // safely listen form to match terms
        $("#search").on("submit", this.score);
        // activate submit only if not term field empty
        $("#term").on("keyup", this.submitable);
    },

    submitable : function (e){
        // prevent form submit
        e.preventDefault();

        var $input  = $(this),
            $buttn  = $input.parent().find("button");

        if( $input.val() == "" || $input.val().length < 3 ) $buttn.attr("disabled", true);
        else $buttn.attr("disabled", false);

    },

    informClickable : function( e ){
        // set context:
        var ctx = ZGTC.Browse;

        ctx.editable = 1;
        console.log("editor stuff ready, we can inform user images are clickable");

        $("#clickable").show();

    },

    score : function (e){
        // prevent form submit
        e.preventDefault();

        // set context:
        var ctx = ZGTC.Browse;

        // start timer
        ctx.filterTimerStart();

        // run through json data to match term in fields
        var term = $("#term").val();

        // clean link states:
        ctx.cleanStates();
        ctx.showInfo( term, "term" );

        if(term != "") {
            var re = new RegExp(term,"gi");
            var data = ctx.JSON;
            // build copy of original object applying weighted scores
            ctx.scored = data.map(function(item){
                var score = 0;
                // 10000 points for term in itemtitle
                score = (item.itemtitle.match(re)!=null)?score+10000:score;
                // 1000  points for term in itembody
                score = (item.itembody.match(re)!=null)?score+1000:score;
                // 100   points for term in keywords
                score = (item.keywords.match(re)!=null)?score+100:score;
                // 10    points for term in chapter
                score = (item.chapter.match(re)!=null)? score+10:score;
                // 1     points for term in book
                score = (item.book.match(re)!=null)?    score+1:score;
                // modify total score for this item, then return
                return {
                    "id": item.id,
                    "logofile": item.logofile,
                    "logolink": item.logolink,
                    "book": item.book,
                    "chapter": item.chapter,
                    "page": item.page,
                    "itemtitle": item.itemtitle,
                    "itembody": item.itembody,
                    "keywords": item.keywords,
                    "score": score // not original but weighted by search term match
                }
            });
        }

        // remove items with score zero
        ctx.clean();

        // publish to stats
        $.publish( "canvas.stats", {"Category":"browse images", "Action":"search term", "Detail":term} );
    },

    clean : function(){
        // set context:
        var ctx = ZGTC.Browse;

        // take only items with score > 0
        ctx.scored = ctx.scored.filter(function( item ){
            return item.score > 0;
        });

        // sort data according to scored match to search term
        ctx.sort();
    },

    sort : function(){
        // set context:
        var ctx = ZGTC.Browse;

        // get index order of scored array ordered by most to least value
        ctx.scored.sort(function (a, b) {
            if (a.score > b.score) {
                return -1;
            }
            if (a.score < b.score) {
                return 1;
            }
            return 0;
        });

        // render scored data
        ctx.render( ctx.scored );
    },

    // ------------------------------------------------------------------------------------ OPTION 2. FILTER ENGINE
    buildstructure : function(){
        // set context:
        var ctx  = ZGTC.Browse,
            data = ctx.JSON;

        for (var i = 0, len = data.length; i < len; i++) {
            var book = data[i].book,
                chap = data[i].chapter;

            if( ctx.books[book] == undefined ) {
                ctx.books[book] = {};
                ctx.linx.push( book );
                ctx.Books.push( book );
            }
            if( ctx.books[book][chap] == undefined ) {
                ctx.books[book][chap] = chap;
                ctx.linx.push( book+"|#|"+chap )
                ctx.Chaps.push( book+"|#|"+chap );
            }
        }
        // console.log( ctx.linx, ctx.Books, ctx.Chaps );

        // remove single books or chapters
        ctx.removeSingles();
    },

    removeSingles : function(){
        // set context:
        var ctx  = ZGTC.Browse,
            BLN  = ctx.Books.length;

        // if we just have one book, dont make it a link, remove from linx
        /*
        if(BLN == 1) {
            var pos = ctx.linx.indexOf( ctx.Books[0] );
            ctx.linx.splice(pos, 1);
        }
        */

        // if one book has one chapter (but several books present), dont make it a link, just link from book
        // if(BLN > 1) {
            for (var i = 0; i < BLN; i++) {
                var counter = 0,
                    index = null;
                for (var k = 0, LLN = ctx.linx.length; k < LLN; k++) {
                    if( ctx.linx[k].indexOf(ctx.Books[i]+"|#|") == 0 ) {
                        index = k;
                        counter++;
                    }
                    if(k == (LLN-1) && counter == 1){
                        ctx.linx.splice(index, 1);
                    }
                }
            }
        // }

        // listen activation links even if not yet in html
        $(document).on("click", ".filter", ctx.listenActivations);

        // we have books/chapter structure, build links now
        ctx.buildlinks();

        // prefill filtrd array with all data
        ctx.filtrd = ctx.JSON;
    },

    buildlinks : function(){
        // set context:
        var ctx  = ZGTC.Browse,
            linx = ctx.linx,
            len  = linx.length,
            buff = "";

        for (var i = 0; i < len; i++) {
            var entry   = linx[i],
                isBook  = entry.indexOf("|#|") == -1,
                a,b,c;

            if(isBook){
                buff += '<a href="#" class="filter filter-book" title="'+entry+'" onclick="ZGTC.Browse.filter(\''+entry+'\')">'+entry+'</a>';
            }
            else {
                a = entry.split("|#|");
                b = a[0];
                c = a[1];
                buff += '<a href="#" class="filter filter-chapter" title="'+b+': '+c+'" onclick="ZGTC.Browse.filter(\''+b+'\', \''+c+'\')">'+c+'</a>';
            }
        }
        $("#menu").append( buff );
        setTimeout(function(){
            // activate first chapter link, or book
            if($(".filter-chapter").length > 0) $(".filter-chapter").first().click();
            else $(".filter-book").first().click();
        },
        //force waiting till events queue is done
        0);
    },

    filter : function( book, chapter){
        // set context:
        var ctx = ZGTC.Browse;
        var what = book+" "+chapter;

        // start timer
        ctx.filterTimerStart();

        // take only items that match book, chapter
        ctx.filtrd = ctx.filtrd.filter(function( item ){
            if(chapter) {
                return item.book == book && item.chapter == chapter;
            }
            else {
                return item.book == book;
            }
        });

        // render scored data
        ctx.render( ctx.filtrd );

        // publish to stats
        $.publish( "canvas.stats", {"Category":"browse images", "Action":"menu link", "Detail":what} );
    },

    // ------------------------------------------------------------------------------------ SEARCH/FILTER INFO
    listenActivations : function(e){
        // set context:
        var ctx = ZGTC.Browse;

        ctx.cleanStates();
        ctx.cleanForm();
        $(this).addClass("active");
        ctx.showInfo( $(this).text(), "links" );
    },

    cleanStates : function(){
        $(".filter").removeClass("active");
        $("#thumbsboxfilter").html();
    },

    cleanForm : function(){
        $("#term").val("");
        $("#thumbsboxfilter").html();
    },

    showInfo : function( what, type ){
        console.log("filter by ", what);
        $("#thumbsboxfilter").html( EditorMessages.filtby +": "+ what );
    },

    // ------------------------------------------------------------------------------------ RENDER + RESET FILTER
    render : function( what ){
        // set context:
        var ctx = ZGTC.Browse,
            comp;

        // end timer
        ctx.filterTimerEnd();

        // store what to render to accessible array so we can swap tpl when editor js ready
        ctx.what = what;

        // render view using underscore
        if(ctx.editable == 0) {
            console.log("editor stuff not ready, use small tpl");
            comp = _.template(ctx.tplmin, {items : ctx.what});
        }
        else {
            console.log("editor stuff ready, use full tpl");
            comp = _.template(ctx.tpl, {items : ctx.what});
        }
        $("#thumbs").html( comp );

        // reset for next filter/search
        ctx.reset();
    },

    activateThumbLinks : function( e ){
        // set context:
        var ctx = ZGTC.Browse,
            comp;

        ctx.editable = 1;
        console.log("editor stuff ready, we can send thumbs to editor mode");

        // re-render using full tpl:
        comp = _.template(ctx.tpl, {items : ctx.what});
        $("#thumbs").html( comp );

    },

    reset : function(){
        // set context:
        var ctx = ZGTC.Browse;

        // reset scored/filtrd subsets
        ctx.scored = ctx.JSON;
        ctx.filtrd = ctx.JSON;
    }
};


/*!
------------------------------------------------------------------------------------------------------
                                                                                        MODALS MODULE
 ----------------------------------------------------------------------------------------------------- */

ZGTC.Modals = {
    init : function(){
        // add modal screen
        ZGTC.CACHE.BODY.append("<div id='modalbox' class='closed'><div id='modalclosewrap'><div id='modalclosecont'><div id='modalclose'><span class='icon icon-close'></span><span></span></div></div></div><div id='modalwrap'><div id='modalcont'><div id='modalhtml'></div></div></div></div>");

        // cache elements
        ZGTC.CACHE.MODAL      = $("#modalbox");
        ZGTC.CACHE.MODALWRAP  = $("#modalwrap");
        ZGTC.CACHE.MODALCONT  = $("#modalcont");
        ZGTC.CACHE.MODALHTML  = $("#modalhtml");
        ZGTC.CACHE.MODALCLOSE = $("#modalclose");


        // listen for clicks on close modal
        ZGTC.CACHE.MODALCLOSE.on("click", function(e){
            e.preventDefault();
            ZGTC.Modals.hideModal();
        });

        // listen for clicks to open modal links
        ZGTC.CACHE.DOC.on("click", ".modal", ZGTC.Modals.clicked);

        // listen for clicks inside unexistent modal text to close modal itself
        ZGTC.CACHE.DOC.on("click", ".close-button, .close-modal-link", ZGTC.Modals.hideModal);
    },

    clicked : function(e){
        e.preventDefault();

        ZGTC.Modals.showModal();
        ZGTC.Modals.get(this.href);
        // publish to stats
        $.publish( "canvas.stats", {"Category":"browse images", "Action":"show modal", "Detail":this.href} );
    },

    showModal : function(){
        var W   = ZGTC.CACHE.W,
            w   = W.width(),
            h   = W.height();

        ZGTC.CACHE.MODAL.removeClass("closed").addClass("opened");
        ZGTC.CACHE.BODY.addClass("modalview");
        //console.log("------------------------- showModal");
    },

    hideModal : function(){
        ZGTC.CACHE.MODAL.removeClass("opened").addClass("closed").html();
        ZGTC.CACHE.BODY.removeClass("modalview");
        //console.log("------------------------- hideModal");
    },

    get : function( URL ){
        var PAR = (URL.indexOf("?") > -1)?"&":"?",
            AJX = "ajax=true";
        $.get(URL+PAR+AJX, function( data ) {
            ZGTC.CACHE.MODALHTML.html( data );
            ZGTC.CACHE.MODALCONT.scrollTop(0);
        }).fail(function() {
            console.log( "ERROR GETTING DATA" );
        });
    }
};

/*!
------------------------------------------------------------------------------------------------------
                                                                                        UTILS MODULE
 ----------------------------------------------------------------------------------------------------- */

ZGTC.Cookies = {
    createCookie : function(name, value, days){
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        } else
            expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    },
    getCookie : function(c_name) {
        if (document.cookie.length > 0) {
            c_start = document.cookie.indexOf(c_name + "=");
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1;
                c_end = document.cookie.indexOf(";", c_start);
                if (c_end == -1) {
                    c_end = document.cookie.length;
                }
                return unescape(document.cookie.substring(c_start, c_end));
            }
        }
        return "";
    }
};

/*!
------------------------------------------------------------------------------------------------------
                                                                                       START NAVIGATOR
 ----------------------------------------------------------------------------------------------------- */
$(function() {
    console.log(ZGTC.Cookies.getCookie('logged') !== 'true');
    if ($('#login').length > 0 && ZGTC.Cookies.getCookie('logged') !== 'true') {
        $('#login').on('submit', function (event) {
            event.preventDefault();
            str = $('#login-user').val() +';'+ $('#login-password').val();

            if (str !== ';' && loginData.indexOf(str) > -1) {
                $('#login-overlay').hide();
                $('#login').hide();
                $('img.float-left').hide();
                $('#data-pass').val($('#login-password').val());
                $('#data-user').val($('#login-user').val());
                ZGTC.Cookies.createCookie('logged', $('#login-user').val(), 365);
            } else {
                $('#login-error').html(loginError).show();
            }
            return false;
        });
    }
});

// ---------------------------------------------------------------- async load canvas editor
ZGTC.Scout.init(".editor");

// ---------------------------------------------------------------- CACHE STUFF
ZGTC.CACHE.init();

// ---------------------------------------------------------------- STANDARIZE + CACHE EVENTS
ZGTC.CACHE.standarizeEvents();

// ---------------------------------------------------------------- ORIENTATION/MQ
ZGTC.Utils.setOrientationBreakpoints();

// ---------------------------------------------------------------- rAF decoupling
ZGTC.CACHE.W.on("orientationchange", function(){
    ZGTC.Utils.setOrientationBreakpoints();
});

// ---------------------------------------------------------------- toggle full text
$(document).on('click', '.thumbinfoikon', function(){
    var $caption = $(this).closest('figcaption');
    if($caption.hasClass('showing')){
        $caption.removeClass('showing');
    }
    else {
        $caption.addClass('showing');
    }
});

// ---------------------------------------------------------------- prepare modals
ZGTC.Modals.init();

// ---------------------------------------------------------------- run browse images
ZGTC.Browse.init();
