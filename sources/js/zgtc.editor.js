

/*!
------------------------------------------------------------------------------------------------------
                                                                                         EDITOR MODULE
 ----------------------------------------------------------------------------------------------------- */
ZGTC.Editor = {

    filterList  : ['brightness', 'matrix', 'color', 'grayscale'],
    saveProps   : ['selectable', 'evented', 'hasControls', 'hasBorders', 'filters'],
    itemtitle   : null,
    needSliders : true,
    genericLogo : "",
    cropXi      : null,
    cropXe      : null,
    cropYi      : null,
    cropYe      : null,
    cropW       : null,
    cropH       : null,
    origW       : null,
    origH       : null,


    // ------------------------------------------------------------------------------------ INIT MONSTER
    init : function( id, title, logofile, logolink ){
        var context = ZGTC.Editor;

        // save title into context so we can give download file a name
        // replace spaces with underscores
        context.itemtitle = title.replace(/ /g, '_');

        // swap body class
        ZGTC.CACHE.BODY.removeClass("mode-navigator").addClass("mode-editor");

        // reset globals
        context.resetGlobals();

        // start through image ID
        context.run( id );

        // check if there are new fonts to append into select
        // avoids iPad bug that adds them only on second focus
        context.instantiateAsyncFonts();

        // save original logo
        context.saveGenericLogoHTML();

        // if logofile/logolink are defined, swap original logo
        if(logofile || logolink) {
            context.addSpecificLogo( logofile, logolink );
        }
    },

    saveGenericLogoHTML : function(){
        var context = ZGTC.Editor;

        context.genericLogo = $("#logo").html();
    },

    addSpecificLogo : function( logofile, logolink ){
        var context = ZGTC.Editor;
        context.genericLogo = $("#logo").html();

        if(!logolink) {
            $("#logo").html( "<img src='public/logos/"+logofile+"' alt='logo'>" );
        }
        else $("#logo").html( "<a href='"+logolink+"' target='_blank'><img src='public/logos/"+logofile+"' alt='logo'></a>" );
    },

    retrieveLogo : function(){
        var context = ZGTC.Editor;
        $("#logo").html( context.genericLogo );
    },

    end : function(){
        var context = ZGTC.Editor;

        // swap body class
        ZGTC.CACHE.BODY.removeClass("mode-editor").addClass("mode-navigator");

        // reset globals
        context.resetGlobals();

        // unlisten events
        context.unlisten();

        // prepare spinner again
        $("#editorloading").show();

        // swap original logo again
        context.retrieveLogo();

        // publish to stats
        $.publish( "canvas.stats", {"Category":"editor", "Action":"close editor", "Detail":context.itemtitle} );
    },

    resetGlobals : function(){
        var context = ZGTC.Editor;

        // console.log("resetGlobals");

        if(context.canvas) {
            context.canvas.clear();
            // console.log("finished clear", context.canvas._objects);
        }
        if(context.img) context.img.remove();
        context.img     = null;
        if(context.imgref) context.imgref.remove();
        context.imgref  = null;
        if(context.tmp) context.tmp.remove();
        context.tmp     = null;
        context.mode    = null;
        context.text    = null;
        context.x0      = null;
        context.y0      = null;

        // empty chronos
        $.publish("reset.chronostate");
    },

    run : function( id ){
        var context = ZGTC.Editor;

        // polyfill range inputs, only once
        if(context.needSliders) {
            $('.slider').each(function(){
                var $ranger = $(this);
                $ranger.noUiSlider({
                    start: $ranger.data("range-value"),
                    step: $ranger.data("range-step"),
                    connect: "lower",
                    direction: 'rtl',
                    orientation: "vertical",
                    range: {
                        'min': $ranger.data("range-min"),
                        'max': $ranger.data("range-max")
                    },
                    format: wNumb({decimals:0})
                });
                $ranger.Link('lower').to('-inline-<div class="tooltip"></div>');
            });
            context.needSliders = false;
        }



        // instantiate all color inputs
        if(!context.canvas) context.colors();

        // create editor
        context.sizeCanvas( id );
    },

    colors : function(){
        var pickers = {
            appendTo: '#text',
            cancelText : EditorMessages['cancel'],
            chooseText : EditorMessages['ok'],
            showPalette: true,
            hideAfterPaletteSelect: true,
            palette: PALETTE,
            showAlpha: true,
            change : ZGTC.Editor.pickerChanged
        };
        $("#textcolor").spectrum(pickers);
        pickers.appendTo = '#line';
        $("#linecolor").spectrum(pickers);
        pickers.appendTo = '#area';
        pickers.allowEmpty = true;
        $("#strokecolor, #fillcolor").spectrum(pickers);

        var blend = {
            appendTo: '#colorfilter',
            cancelText : EditorMessages['cancel'],
            chooseText : EditorMessages['ok'],
            showPalette: true,
            hideAfterPaletteSelect: true,
            palette: PALETTE,
            showAlpha: true,
            change : ZGTC.Editor.colorFilter
        };
        $("#Blend").spectrum(blend);

        // set default colors
        $("#strokecolor, #linecolor").spectrum("set", PALETTEMAIN);
        $("#textcolor, #Blend").spectrum("set", PALETTEMAIN);
        $("#fillcolor").spectrum("set", "transparent");
    },

    cloneThumb : function( id ){
        $( "#thumbs_id_"+id ).clone().appendTo( "#editarea" );
        // remove cloned id
        $( "#editarea #thumbs_id_"+id ).attr("id","cloned_thumb");
        var ct = $( "#cloned_thumb");
        var w = ct.width();
        var h = ct.height();
        $( "#cloned_thumb").remove();

        return w+"x"+h;
    },

    sizeCanvas : function( id ){
        var context = ZGTC.Editor;
        var self = this;

        // clone img into actions
        var size = context.cloneThumb( id ).split("x");
        var w = size[0];
        var h = size[1];
        var thRatio = w/h;

        // get max editor space
        var pad  = 20;
        var wW   = ZGTC.CACHE.W.width();
        var wH   = ZGTC.CACHE.W.height();
        var aW   = $(".actions").outerWidth() + (2 * pad);
        var hsum;
        // landscape
        if(wW > wH) {
            // actions + subactions + secondary actions in right side + half piece room to make it centered
            hsum = ZGTC.CACHE.EA.outerWidth() - (3.5 * aW);
        }
        // portrait
        else {
            // actions + subactions
            hsum = ZGTC.CACHE.EA.outerWidth() - (2 * aW);
        }
        var vsum = ZGTC.CACHE.EA.outerHeight() - (2 * pad);
        var edRatio = hsum/vsum;

        // Date url trick so we can detect onload
        var img = $("<img />").attr("src","./public/img/"+id+ ".png?" + new Date().getTime()).attr("id","originalimg");
        $("#editarea").append(img);
        context.imgref = $("#originalimg");
        context.imgref.one("error", function(){
            console.log("error pre/loading image");
        });

        // .one because we might reuse this code with several IDs
        context.imgref.one("load", function() {
            var imgOrigWidth = context.imgref.width();

            // console.log("context.imgref loaded: w, h, thRatio, edRatio, hsum", w, h, thRatio, edRatio, hsum);
            if( thRatio > edRatio ){
                // console.log("image aspect > editor canvas aspect");
                // console.log("make image width 100%");
                context.imgref.width( hsum ).height("auto");
            }
            else if( thRatio < edRatio ){
                // console.log("editor canvas aspect > image aspect");
                // console.log("make image height 100%");
                context.imgref.height( vsum ).width("auto");
            }

            // instanciate canvas if needed
            if(!context.canvas) {
                // create empty canvas element
                $("#editarea").append('<canvas id="canvas"></canvas>');

                context.canvas = new fabric.Canvas('canvas', { selection: false });
            }

            // image is loaded for sure now
            context.img = new fabric.Image(context.imgref[0], {
                selectable: false,
                evented: false,
                hasControls: false,
                hasBorders: false,
                left: 0,
                top: 0
            });

            // add image to canvas
            context.canvas.add(context.img);

            // make canvas adjust to optimal image size
            context.canvas.setWidth( context.img.getWidth() );
            context.canvas.setHeight( context.img.getHeight() );

            // save canvas size so we can redo/undo from crops
            context.origW = context.img.getWidth();
            context.origH = context.img.getHeight();
            context.ratio = imgOrigWidth / context.origW;

            // remove original image, we dont need it anymore:
            $("#originalimg").remove();

            // render
            context.canvas.renderAll();

            // save state
            context.save(EditorMessages["init"]);
            // publish to stats
            $.publish( "canvas.stats", {"Category":"editor", "Action":"init", "Detail":context.itemtitle} );

            // fix IE8 shit
            if( ZGTC.Utils.isCry() ) {
                $(".canvas-container").attr("id", "canvas-container");
                $("#canvas-container").trigger("click");
                // IE8 cant get datauri to even save to file, avoid download button
                $("#downloadme").hide();
                // IE8 cant get image filters, avoid them
                $("#filtreme").hide();
                // IE8 cant do the crop stuff, hide it
                $("#cropme").hide();
            }

            // hide spinner now that everything is rendered ok
            $("#editorloading").hide();

            // listen events
            context.listen();

            // create a pubsub channel (decoupled from Chronos module)
            $.subscribe("rewrite.chronostate", context.rewriteRecord);
        });
    },

    listen : function(){
        this.canvas.on('path:created', this.oPathCreated);
        this.canvas.on('text:changed', this.oTextChanged);
        this.canvas.on('text:editing:exited', this.oTextExit);
        this.canvas.on('mouse:down', this.oDown);
        this.canvas.on('object:modified', this.oModified);
        $("#width, #swidth, #fs").on("change", this.widthChanged);
        $(document).on("click", ".fontselector", this.fontChanged);
        $(document).on("keydown", this.oKey);
        // filters
        $("#BW").on("change", this.bwFilter);
        $("#Brightness").on("change", this.brightFilter);
        $("#Contrast").on("change", this.contrastFilter);
    },

    unlisten : function(){
        this.canvas.off('text:changed');
        this.canvas.off('text:editing:exited');
        this.canvas.off('mouse:down');
        this.canvas.off('object:modified');
        $("#width, #swidth, #fs").off("change");
        $(document).off("click", ".fontselector");
        $(document).off("keydown");
        // filters
        $("#BW").off("change");
        $("#Brightness").off("change");
        $("#Contrast").off("change");
        // unsubscribe chronostate
        $.unsubscribe("rewrite.chronostate");
    },

    // ------------------------------------------------------------------------------------ FONTS / TEXT
    prepareFont : function( fontFamily ){
        $("#fontfamily").append('<a class="fontselector" href="#" data-font="'+fontFamily+'" style="font-family:'+fontFamily+'">'+fontFamily+'</a>');
    },

    instantiateAsyncFonts : function(){
        var context = ZGTC.Editor,
            slow = WebFontBuffer.length;

        for (var i = 0; i < slow; i++) {
            var font = WebFontBuffer.pop();
            context.prepareFont( font );
        }
    },

    oTextChanged : function(e){
        var context = ZGTC.Editor,
            active  = context.getActive();

        // save text content into context to be saved at exit
        context.text = active.text;
    },

    oTextExit : function(e){
        var context = ZGTC.Editor,
            active  = context.getActive();

        // retrieve text content from buffer and save if not null
        if(context.text != null) {
            // save/override state
            // (bit of a hack to avoid saving 1px changes in succession)
            if(Chronos.states[Chronos.current]["title"] == EditorMessages["new"]+EditorMessages["text"]) {
                context.save(EditorMessages["new"]+EditorMessages["text"], 1);
            }
            else context.save(EditorMessages["new"]+EditorMessages["text"]);
            // publish to stats
            $.publish( "canvas.stats", {"Category":"editor", "Action":"new text", "Detail":context.itemtitle} );
        }
        // reset
        context.text = null;
        context.resetMode();
    },

    // ------------------------------------------------------------------------------------ INSERT MODES
    oKey : function(e){
        var context = ZGTC.Editor,
            active  = context.getActive();

        // remove active-selected object if user presses back key
        if( e.which == 8 && typeof active == "object" ) {
            context.removeActive();
            // prevent fucking up with browser navigation
            e.preventDefault();
            e.stopPropagation();
        }
    },

    getActive : function(){
        return ZGTC.Editor.canvas._activeObject;
    },

    removeActive : function(){
        var context = ZGTC.Editor,
            active  = context.getActive(),
            oType   = active.type;

        active.remove();

        // render again
        context.canvas.renderAll();

        // save state
        context.save(EditorMessages["del"]+EditorMessages[oType]);
        // publish to stats
        $.publish( "canvas.stats", {"Category":"editor", "Action":"remove "+oType, "Detail":context.itemtitle} );
    },

    setMode : function( button, mode ){
        var context = ZGTC.Editor;
        // endFreeDrawing
        this.endFreeDrawing();
        // endCrop
        this.endCrop();

        if( $( button ).hasClass("active-button") ) {
            // was using this mode, get out
            context.resetMode();
            $( button ).removeClass("active-button");
            context.hideSubactions();
        }
        else {
            context.resetMode();
            context.canvas.discardActiveObject();
            // fix text mode for IE
            if( ZGTC.Utils.isIE() && mode == "i-text") mode = "text";
            context.mode = mode;
            $( button ).addClass("active-button");
            context.showSubactions( mode );
            if( mode == "path") context.startFreeDrawing();
            if( mode == "crop") context.startCrop();
        }
    },

    resetMode : function(){
        var context = ZGTC.Editor;
        context.mode = null;
        $(".active-button").removeClass("active-button");
        // endFreeDrawing
        context.endFreeDrawing();
        // end crop
        context.endCrop();
        // hide
        context.hideSubactions();
    },

    showSubactions : function( shape ){
        //console.log("SHOW SUBACTIONS FOR ", shape);
        this.hideSubactionsMain();
        if(shape === "text" || shape === "i-text") {
            // instantiate async fonts again, just in case
            ZGTC.Editor.instantiateAsyncFonts();
            $("#subactions").addClass("active-subaction---text");
            $("#text").addClass("active");
        }
        if(shape === "line") {
            $("#subactions").addClass("active-subaction---line");
            $("#line").addClass("active");
        }
        if(shape === "arrow") {
            $("#subactions").addClass("active-subaction---arrow");
            $("#line").addClass("active");
        }
        if(shape === "path") {
            $("#subactions").addClass("active-subaction---path");
            $("#line").addClass("active");
        }
        if(shape === "rect") {
            $("#subactions").addClass("active-subaction---rect");
            $("#area").addClass("active");
        }
        if(shape === "circle") {
            $("#subactions").addClass("active-subaction---circle");
            $("#area").addClass("active");
        }
        if(shape === "imgfilters") {
            $("#subactions").addClass("active-subaction---imgfilters");
            $("#imgfilters").addClass("active");
        }
    },

    hideSubactions : function(){
        //console.log("HIDE SUBACTIONS");
        this.hideSubactionsMain();
        $(".subactions-type").removeClass("active");
    },

    hideSubactionsMain : function(){
        $("#subactions").removeClass("active-subaction---text active-subaction---line active-subaction---arrow active-subaction---path active-subaction---rect active-subaction---circle active-subaction---imgfilters");
    },

    // ------------------------------------------------------------------------------------ UNDO / REDO
    undo : function(){
        var context = ZGTC.Editor;

        // do action
        Chronos.undo();

        // publish to stats
        $.publish( "canvas.stats", {"Category":"editor", "Action":"undo", "Detail":context.itemtitle} );
    },

    redo : function(){
        var context = ZGTC.Editor;

        // do action
        Chronos.redo();

        // publish to stats
        $.publish( "canvas.stats", {"Category":"editor", "Action":"redo", "Detail":context.itemtitle} );
    },

    // ------------------------------------------------------------------------------------ SAVE / DOWNLOAD / PRINT
    prepareDownload : function(){
        var context = ZGTC.Editor,
            datauri, url;

        if( ZGTC.Utils.isIE() || ZGTC.Utils.isSafari() ) {
            $("#downloadme").attr("href", "download/"+context.itemtitle+".png");
        }
        else {
            datauri = ZGTC.Editor.getDataURI();
            url = datauri.replace(/^data:image\/png/, 'data:application/octet-stream');
            // change url of download button
            $("#downloadme").attr("href", url);
            $("#downloadme").attr("download", context.itemtitle+".png");
        }
        // publish to stats
        $.publish( "canvas.stats", {"Category":"editor", "Action":"download", "Detail":context.itemtitle} );
    },

    getDataURI : function(){
        var context = ZGTC.Editor;

        // unselect elements in canvas
        context.canvas.discardActiveObject();

        // get datauri
        var datauri = context.canvas.toDataURL({
            format: 'png',
            multiplier: context.ratio // make it depend on zoom to download original image size
        });

        return datauri;
    },

    print : function(){
        var context = ZGTC.Editor;

        // unselect elements in canvas
        context.canvas.discardActiveObject();
        // publish to stats
        $.publish( "canvas.stats", {"Category":"editor", "Action":"print", "Detail":context.itemtitle} );
        // print
        window.print();
    },

    removeFiltersFromJSON : function( json ){
        var obs = json.objects;
        for (var i = 0, len = obs.length; i < len; i++) {
            if(obs[i].type == "image") obs[i].filters = [];
        }
    },

    save : function( what, discard ){
        var context = ZGTC.Editor,
            datauri, action;

        if( ZGTC.Utils.isIE() && !ZGTC.Utils.isCry() || ZGTC.Utils.isSafari() ) {
            //IE9+ since IE8 does not get save button, save datauri as real tmp image
            datauri = ZGTC.Editor.getDataURI();

            $.ajax({
                contentType: "application/upload",
                type: "POST",
                beforeSend: function( xhr ) {
                    xhr.overrideMimeType( "application/upload" );
                },
                url: "download/save.php",
                data: JSON.stringify({img: datauri, name: context.itemtitle}),
                dataType: "text",
                cache: false
            });
        }

        // save state to Chronos
        if(discard) action = "discard.chronostate";
        else action = "save.chronostate";
        var json = this.canvas.toJSON( ZGTC.Editor.saveProps );
        this.removeFiltersFromJSON( json );
        var string = JSON.stringify( json );
        // console.log(action, what);
        $.publish(action, {
            "title": what,
            "content": string
        });
    },

    rewriteRecord : function( e, JSON ){
        // e not used, needed as per "jQuery Tiny Pub/Sub" requirement
        var context = ZGTC.Editor,
            canvas  = context.canvas;
        /*canvas.loadFromJSON( JSON.content, function(o, obj){
            // removed, TOOOOOO SLOW
            ZGTC.Editor.applyFiltersFromJSON(o, obj);
        } );*/
        canvas.loadFromJSON( JSON.content, canvas.renderAll.bind(canvas) );

        console.log("PUBSUB SYSTEM - LOAD FROM CHRONO STATE", JSON.title);
        if( JSON.title.indexOf(EditorMessages["crop"]) > -1 ){
            console.log("CHRONO STATE DETECTED CROP, RESET DIMENSIONS");
            context.resetCropCanvas();
        }
    },

    // ------------------------------------------------------------------------------------ COLOR / SIZE EDITION
    pickerChanged  : function(e){
        var context = ZGTC.Editor,
            object  = context.canvas._activeObject;

        if( object ) {
            if( object.type == "arrow" || object.type == "line" ) {
                // console.log("pickerChanged line/arrow");
                object.fill = $("#linecolor").val();
                object.stroke = $("#linecolor").val();
            }
            if( object.type == "path" ) {
                // console.log("pickerChanged path");
                object.stroke = $("#linecolor").val();
            }
            if( object.type == "rect" || object.type == "circle" ) {
                object.fill = $("#fillcolor").val();
                object.stroke = $("#strokecolor").val();
            }
            if( object.type == "text" || object.type == "i-text" ) {
                object.fill = $("#textcolor").val();
                object.stroke = $("#textcolor").val();
            }

            // render again
            context.canvas.renderAll();

            // save state
            context.save(EditorMessages["prop"]+EditorMessages["color"]);
            // publish to stats
            $.publish( "canvas.stats", {"Category":"editor", "Action":"modify "+object.type+" color", "Detail":context.itemtitle} );
        }
    },

    widthChanged  : function(e){
        var context = ZGTC.Editor,
            object  = context.canvas._activeObject,
            readID  = $(e.currentTarget).attr("id"),
            prop    = (readID=="swidth" || readID=="width")?"strokeWidth":"fontSize",
            val;

        if( object ) {
            object[prop] = parseInt($("#"+readID).val(), null);

            // render again
            context.canvas.renderAll();

            // save/override state
            // (bit of a hack to avoid saving 1px changes in succession)
            if(Chronos.states[Chronos.current]["title"] == EditorMessages["prop"]+prop) {
                context.save(EditorMessages["prop"]+prop, 1);
            }
            else {
                context.save(EditorMessages["prop"]+prop);
            }
            // publish to stats
            $.publish( "canvas.stats", {"Category":"editor", "Action":"modify "+object.type+" width", "Detail":context.itemtitle} );
        }
    },

    fontChanged  : function(e){
        var context = ZGTC.Editor,
            object  = context.canvas._activeObject,
            prop    = 'fontFamily',
            val;

        $(".fontselector.selected").removeClass("selected");
        $(this).addClass("selected");
        val = $(this).data("font");

        if( object ) {
            object[prop] = val;

            // render again
            context.canvas.renderAll();

            // save/override state
            // (bit of a hack to avoid saving 1px changes in succession)
            if(Chronos.states[Chronos.current]["title"] == EditorMessages["prop"]+prop) {
                context.save(EditorMessages["prop"]+prop, 1);
            }
            else {
                context.save(EditorMessages["prop"]+prop);
            }
            // publish to stats
            $.publish( "canvas.stats", {"Category":"editor", "Action":"modify font", "Detail":context.itemtitle} );
        }
    },

    // ------------------------------------------------------------------------------------ FILTERS
    brightFilter  : function(e){
        var context = ZGTC.Editor,
            obj     = context.canvas.getObjects()[0],
            func    = $(e.currentTarget).attr("id"),
            prop    = func.toLowerCase(),
            val     = $("#"+func).val(),
            propval = {};

        propval[prop] = parseInt(val, 10);

        // apply filter
        context.applyFilterValue(obj, context.getFilterIndex(prop), func, propval);
        // publish to stats
        $.publish( "canvas.stats", {"Category":"editor", "Action":"modify brightness", "Detail":context.itemtitle} );
    },

    contrastFilter  : function(e){
        var context = ZGTC.Editor,
            obj     = context.canvas.getObjects()[0],
            func    = "Convolute",
            prop    = "matrix",
            val     = $("#Contrast").val(),
            propval = {};

        propval[prop] = context.getContrastMatrix( val );

        // apply filter
        context.applyFilterValue(obj, context.getFilterIndex(prop), func, propval);
        // publish to stats
        $.publish( "canvas.stats", {"Category":"editor", "Action":"modify contrast", "Detail":context.itemtitle} );
    },

    getContrastMatrix : function( multiplier ){
        var positive= Math.abs( multiplier ),
            negative= -1 * positive,
            matrix, rest;
        if( multiplier == 0) {
            matrix  = [ 0,0,0,
                        0,1,0,
                        0,0,0 ];
        }
        if( multiplier < 0) {
            // blur, everything adds up to = 1
            // we take simplest form (1/9)
            // to give more blur we would need more than 3x3 matrix
            matrix = [  (1/9),(1/9),(1/9),
                            (1/9),(1/9),(1/9),
                            (1/9),(1/9),(1/9) ];
        }
        if( multiplier > 0) {
            // focus
            // center more force, adjacent to center negative, corners zero
            // technically everything adds up to = 1
            // but we will add some brightness
            matrix  = [ 0,negative,0,
                        negative,(4.1*positive)+1,negative,
                        0,negative,0 ];
        }
        return matrix;
    },

    colorFilter : function(e){
        var context = ZGTC.Editor,
            obj     = context.canvas.getObjects()[0],
            func    = "Blend",
            prop    = "color",
            val     = $("#"+func).val(),
            alpha   = $("#"+func).spectrum("get").toRgb()["a"],
            propval = { "color": val, "mode": "add", "alpha": alpha };

        // apply filter
        context.applyFilterValue(obj, context.getFilterIndex(prop), func, propval);
        // publish to stats
        $.publish( "canvas.stats", {"Category":"editor", "Action":"modify hue blend mode", "Detail":context.itemtitle} );
    },

    bwFilter  : function(e){
        var context = ZGTC.Editor,
            obj     = context.canvas.getObjects()[0],
            func    = "Grayscale",
            prop    = "grayscale",
            val     = $("#BW").is(":checked"),
            propval = {};

        if(val) propval["type"] = prop;
        else propval = false;

        // apply filter
        context.applyFilterValue(obj, context.getFilterIndex(prop), func, propval);
        // publish to stats
        $.publish( "canvas.stats", {"Category":"editor", "Action":"modify b&w to "+val, "Detail":context.itemtitle} );
    },

    getFilterIndex : function( prop ){
        return ZGTC.Editor.filterList.indexOf(prop);
    },

    applyFilterValue  : function(obj, index, fn, propvalue) {
        var context = ZGTC.Editor,
            canvas  = context.canvas,
            prop = "filters";

            if(obj == undefined) {
                obj = context.canvas.getObjects()[0];
                // console.log("applyFilterValue --- obj was undefined, reassign: ", obj);
            }

            // use value or remove previous (ie for checkbox input)
            if(typeof propvalue == "object") obj.filters[index] = new fabric.Image.filters[fn]( propvalue );
            else obj.filters[index] = propvalue;
            // apply to canvas
            obj.applyFilters( canvas.renderAll.bind(canvas) );
    },

    // not in use, tooooooooooooooo slow
    applyFiltersFromJSON  : function( o, obj ) {
        var context = ZGTC.Editor,
            FIL     = obj.filters,
            propval = {};

        // regular objects are ok, we need these to reapply filters to background image object
        if(obj.type == "image") {
            //console.log("=========== reapply filters");
            // check our filterList props ['brightness', 'matrix', 'color', 'grayscale']
            // if any are changed in obj.filters, apply one by one
            // since prop/val is different depending on filter, we do this manually, not using a loop

            // brightness, ok
            if(FIL[0] != undefined){
                propval["brightness"] = FIL[0]["brightness"];
                //console.log( "brightness not empty, reapply to image", propval );
                context.applyFilterValue(obj, 0, "Brightness", propval);
                propval = {};
            }
            else { context.applyFilterValue(obj, 0, "Brightness", undefined); }

            // matrix, ok
            if(FIL[1] != undefined){
                propval["matrix"] = FIL[1]["matrix"];
                //console.log( "matrix not empty, reapply to image", propval );
                context.applyFilterValue(obj, 1, "Convolute", propval);
                propval = {};
            }
            else { context.applyFilterValue(obj, 1, "Convolute", undefined); }

            // color
            if(FIL[2] != undefined){
                propval["color"] = FIL[2]["color"];
                propval["mode"] = FIL[2]["mode"];
                propval["alpha"] = FIL[2]["alpha"];
                //console.log( "color not empty, reapply to image", propval );
                context.applyFilterValue(obj, 2, "Blend", propval);
                propval = {};
            }
            else { context.applyFilterValue(obj, 2, "Blend", undefined); }

            // grayscale, ok
            if(FIL[3] != undefined){
                if(FIL[3] != false) propval["type"] = "grayscale";
                else propval = false;
                //console.log( "grayscale not empty, reapply to image", propval );
                context.applyFilterValue(obj, 3, "Grayscale", propval);
                propval = {};
            }
            else { context.applyFilterValue(obj, 3, "Grayscale", undefined); }
        }
    },

    // ------------------------------------------------------------------------------------ FREE DRAWING
    startFreeDrawing : function(){
        // console.log("startFreeDrawing");
        var context = ZGTC.Editor,
            canvas  = context.canvas;

        // freeDrawingBrush setup
        canvas.freeDrawingBrush.color = $("#linecolor").val();
        canvas.freeDrawingBrush.width = parseInt($("#width").val(),null);
        // enable drawing
        canvas.isDrawingMode = true;
    },

    endFreeDrawing : function(){
        // console.log("endFreeDrawing");
        var context = ZGTC.Editor,
            canvas  = context.canvas;

        canvas.isDrawingMode = false;
    },

    oPathCreated : function(e){
        var context = ZGTC.Editor;

        // endFreeDrawing
        context.endFreeDrawing();

        // save state
        context.save(EditorMessages["new"]+EditorMessages["pencil"]);
        // publish to stats
        $.publish( "canvas.stats", {"Category":"editor", "Action":"new pencil", "Detail":context.itemtitle} );

        // reset
        context.resetMode();
    },

    // ------------------------------------------------------------------------------------ CROP MODE
    startCrop : function(){
        // console.log("start crop mode");
        var context = ZGTC.Editor,
            canvas  = context.canvas;

    },

    endCrop : function(){
        // console.log("end crop mode");
        var context = ZGTC.Editor,
            canvas  = context.canvas;

        // remove crop area visualization layers
        $("#cropBox").remove();
    },

    oCropDown : function(options){
        // console.log('EVENT - oCropDown ');
        var context = ZGTC.Editor;
        var pointer = context.canvas.getPointer(options.e);

        context.cropXi = pointer.x;
        context.cropYi = pointer.y;

        // add crop area visualization layers
        $(".canvas-container").append("<div id='cropBox'><div class='crop-limits crop-t'></div><div class='crop-limits crop-r'></div><div class='crop-limits crop-b'></div><div class='crop-limits crop-l'></div></div>");

        //listen for mouse/drag events to setup crop area:
        context.canvas.on('mouse:move', context.oCropMoved);
        context.canvas.on('mouse:up', context.oCropUpped);
    },

    oCropMoved : function(options){
        // console.log('EVENT - oCropMoved ');

        var context = ZGTC.Editor;
        var pointer = context.canvas.getPointer(options.e);
        var totalW  = $(".canvas-container").width();
        var totalH  = $(".canvas-container").height();

        context.cropXe = pointer.x;
        context.cropYe = pointer.y;
        context.cropW  = context.cropXe - context.cropXi;
        context.cropH  = context.cropYe - context.cropYi;

        // update crop area visualization
        $('.crop-t').css( { "height": context.cropYi + "px" } );
        $('.crop-r').css( { "top": context.cropYi + "px", "width": totalW - context.cropXe + "px", "height": context.cropH + "px", "left": context.cropXe + "px" } );
        $('.crop-b').css( { "top": context.cropYe + "px", "height": totalH - context.cropYe + "px" } );
        $('.crop-l').css( { "top": context.cropYi + "px", "width": context.cropXi + "px", "height": context.cropH + "px" } );
    },

    oCropUpped : function(options){
        // console.log('EVENT - oCropUpped ');

        var context = ZGTC.Editor;

        // stop listening moves:
        context.canvas.off('mouse:move');
        context.canvas.off('mouse:up');

        // crop
        context.cropCanvas();

        // reset mode
        context.resetMode();
    },

    cropCanvas : function(){
        var context = ZGTC.Editor,
            canvas  = context.canvas,
            cW      = canvas.getWidth(),
            picture = context.img,
            x       = context.cropXi,
            y       = context.cropYi,
            w       = context.cropW,
            h       = context.cropH,
            factor  = cW / w;

        // move image
        picture.setTop(  -y * factor );
        picture.setLeft( -x * factor );
        // rescale image
        picture.scale( factor );
        // resize canvas
        canvas.setWidth(  w * factor );
        canvas.setHeight( h * factor );
        // render again
        canvas.renderAll();
        // save state
        context.save(EditorMessages["new"]+EditorMessages["crop"]+" w:"+w+"x"+h);
        // publish to stats
        $.publish( "canvas.stats", {"Category":"editor", "Action":"new crop", "Detail":context.itemtitle} );
    },

    resetCropCanvas : function(){
        var context = ZGTC.Editor,
            canvas  = context.canvas,
            cW      = canvas.getWidth(),
            oW      = context.origW,
            picture = context.img,
            factor  = cW / oW;

        // move image to (0,0)
        picture.setTop(  0 );
        picture.setLeft( 0 );
        // rescale image to original size
        picture.scale( factor );
        picture.setWidth(  context.origW );
        picture.setHeight( context.origH );
        // resize canvas   to original size
        canvas.setWidth(  context.origW );
        canvas.setHeight( context.origH );
        // render
        canvas.renderAll();
    },

    // ------------------------------------------------------------------------------------ MODIFY / ADD SHAPES
    oModified : function(options){
        var context = ZGTC.Editor;

        // use when manually modifying an object with editor buttons (ie: color, lineWidth, …)
        // context.canvas.fire('object:modified', { target: context.canvas._activeObject });

        // save state
        context.save(EditorMessages["mod"]+EditorMessages[options.target.type]);
        // publish to stats
        $.publish( "canvas.stats", {"Category":"editor", "Action":"modify "+options.target.type, "Detail":context.itemtitle} );
    },

    oDown : function(options){
        // console.log('EVENT - mouse down ');

        var context = ZGTC.Editor;
        var pointer = context.canvas.getPointer(options.e);

        if(options.target) {
            context.mode = "edition";
            options.target.bringToFront();
            context.showSubactions( options.target.type );
            return false;
        }
        else {
            context.hideSubactions();
        }
        if (context.mode == null || context.mode == "selection") {
            return false;
        }

        //console.log( "context mode: ", context.mode );
        if( context.mode == "text" || context.mode == "i-text" ) {
            var mod = (ZGTC.Utils.isIE())?"":EditorMessages["addok"],
                txt = prompt( EditorMessages["addtxt"] + mod, "" ),
                opts = {
                    fontFamily: $(".fontselector.selected").data("font"),
                    left: pointer.x,
                    top: pointer.y,
                    fill: $("#textcolor").val(),
                    fontSize: parseInt($("#fs").val(),null),
                },
                t;
            // IE uses old Text object, else full Text support
            if( ZGTC.Utils.isIE() ) t = new fabric.Text(txt, opts);
            else t = new fabric.IText(txt, opts);

            context.canvas.add(t);
            context.resetMode();
            context.canvas.renderAll();
            // save state
            context.save(EditorMessages["new"]+EditorMessages["text"]);
            // publish to stats
            $.publish( "canvas.stats", {"Category":"editor", "Action":"new text", "Detail":context.itemtitle} );
        }
        if( context.mode == "arrow" || context.mode == "line" ) {
            var points = [ pointer.x, pointer.y, pointer.x, pointer.y ];
            context.tmp = new fabric.Line(points, {
                originX: "center",
                originY: "center",
                strokeWidth : parseInt($("#width").val(),null),
                stroke: $("#linecolor").val(),
                fill:  $("#linecolor").val()
            });
        }
        if( context.mode == "rect" || context.mode == "circle" ) {
            context.x0 = pointer.x;
            context.y0 = pointer.y;
            var opts = {
                top : pointer.y,
                left : pointer.x,
                width : 1,
                height : 1,
                stroke :  $("#strokecolor").val(),
                strokeWidth : parseInt($("#swidth").val(),null),
                fill :  $("#fillcolor").val()
            }
        }
        if( context.mode == "rect" ) {
            context.tmp = new fabric.Rect(opts);
        }
        if( context.mode == "circle" ) {
            context.tmp = new fabric.Circle(opts);
            context.tmp.radius = 1;
        }

        if(context.mode == "arrow" || context.mode == "line" || context.mode == "rect" || context.mode == "circle"){
            // add tmp shape
            context.canvas.add(context.tmp);
            // since this is a dynamically shaped object listen for mouse/drag events:
            context.canvas.on('mouse:move', context.oMoved);
            context.canvas.on('mouse:up', context.oUpped);
        }
        if(context.mode == "crop"){
            // delegate crop event to specific manager
            context.oCropDown( options )
        }
    },

    oMoved : function(options){
        // console.log('EVENT - mouse move ');

        var context = ZGTC.Editor;
        var pointer = context.canvas.getPointer(options.e);

        //console.log( "context mode: ", context.mode );
        if( context.mode == "arrow" || context.mode == "line" ) {
            context.tmp.set({ x2: pointer.x, y2: pointer.y });
        }
        if( context.mode == "rect" ) {
            var opts = {
                width  : parseInt(pointer.x - context.x0,null),
                height : parseInt(pointer.y - context.y0,null)
            }
            context.tmp.set(opts);
        }
        if( context.mode == "circle" ) {
            context.tmp.set(
                { radius: Math.abs(context.x0 - pointer.x) }
            );
        }

        // render again
        context.canvas.renderAll();
    },

    oUpped : function(options){
        // console.log('EVENT - mouse up ');

        var context = ZGTC.Editor;

        if( context.mode == "arrow" || context.mode == "line" || context.mode == "rect" || context.mode == "circle" ) {
            // insert real object instead of ephemeral
            context.oInsertFinalShape();
        }

        // stop listening moves:
        context.canvas.off('mouse:move');
        context.canvas.off('mouse:up');
        context.x0 = null;
        context.y0 = null;
        context.resetMode();
    },

    oInsertFinalShape : function(){
        var context = ZGTC.Editor;
        var shape;

        // copy ephemeral options
        if( context.mode == "arrow" || context.mode == "line" ) {
            var points = [ context.tmp.x1, context.tmp.y1, context.tmp.x2, context.tmp.y2 ];
            if(context.mode == "arrow") {
                shape = new fabric.Arrow(points);
            }
            if(context.mode == "line") {
                shape = new fabric.Line(points);
            }
        }
        if( context.mode == "rect" ) {
            var opts = {
                top     : context.tmp.top,
                left    : context.tmp.left,
                width   : context.tmp.width,
                height  : context.tmp.height
            }
            shape = new fabric.Rect(opts);
        }
        if( context.mode == "circle" ) {
            var opts = {
                top     : context.tmp.top,
                left    : context.tmp.left,
                radius  : context.tmp.radius
            }
            shape = new fabric.Circle(opts);
        }

        shape.strokeWidth = parseInt(context.tmp.strokeWidth, null);
        shape.fill = context.tmp.fill;
        shape.stroke = context.tmp.stroke;

        context.canvas.add(shape);

        // remove ephemeral shape
        context.tmp.remove();
        context.tmp = null;

        // render again
        context.canvas.renderAll();

        // save state
        context.save(EditorMessages["new"]+EditorMessages[context.mode]);
        // publish to stats
        $.publish( "canvas.stats", {"Category":"editor", "Action":"new "+context.mode, "Detail":context.itemtitle} );
    }
};


/*!
------------------------------------------------------------------------------------------------------
                                                                                          START EDITOR
 ----------------------------------------------------------------------------------------------------- */

// tell navigator that editor is ready to receive click on links
$.publish("ready.editor");

