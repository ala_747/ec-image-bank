

/*!
------------------------------------------------------------------------------------------------------
                                                                                          STATS MODULE
 ----------------------------------------------------------------------------------------------------- */
ZGTC.Stats = {
    USERID : window.USERID,
    SITEID : window.SITEID,

    init : function(){
        var ctx = ZGTC.Stats;
        $.subscribe("canvas.stats", this.save);
        console.log("setting up stats for", ctx.USERID, ctx.SITEID);
    },

    save : function(e, what){
        var ctx = ZGTC.Stats;
        console.log("saving action stats in piwik for", ctx.USERID, ctx.SITEID, " Category", what.Category, "Action", what.Action, "Detail", what.Detail);
        // for instance, piwik:
        _paq.push(['trackEvent', what.Category, what.Action, what.Detail]);
    }
};
ZGTC.Stats.init();