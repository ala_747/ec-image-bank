/*!
fabricJS Arrow object extension for Line
v0.1.0, Sergi Meseguer @zigotica
https://github.com/zigotica/fabricJS.Arrow/
*/

fabric.Arrow = fabric.util.createClass(fabric.Line, {

    type : "arrow",

    initialize: function(points, options) {
        options || (options = { wFactor: 1, hFactor: 2, fill: "black", stroke: "black", strokeWidth: 3 });

        if (!points) {
            points = [0, 0, 0, 0];
        }

        this.callSuper('initialize', options);

        this.set('x1', points[0]);
        this.set('y1', points[1]);
        this.set('x2', points[2]);
        this.set('y2', points[3]);
        this.set('width',  Math.abs(points[2] - points[0]) || 10);
        this.set('height', Math.abs(points[3] - points[1]) || 10);
        this.set('wFactor', options.wFactor);
        this.set('hFactor', options.hFactor);
        this.set('fill', options.fill);
        this.set('stroke', options.stroke);
        this.set('strokeWidth', options.strokeWidth);
    },

    _getCorrectedAngle : function( nearAngle, angle ){
        var dir = (angle - nearAngle > 0)?1:-1;
        return dir * Math.abs(nearAngle - angle);
    },

    _render: function(ctx) {
        this.callSuper('_render', ctx);

        // short aliases
        var w   = this.width;
        var h   = this.height;
        var x1  = this.x1;
        var x2  = this.x2;
        var y1  = this.y1;
        var y2  = this.y2;
        var wF  = this.wFactor;
        var hF  = this.hFactor;
        var sW  = this.strokeWidth;
        var st  = this.stroke;

        // find arrow direction using start/end coords
        var xDirection  = (x1 < x2)?"R":(x1 > x2)?"L":"|";
        var yDirection  = (y1 < y2)?"B":(y1 > y2)?"T":"-";
        var Direction   = xDirection + yDirection;

        // get angle using start/end coords
        var pendent     = (y2-y1)/(x2-x1);
        var angle       = Math.atan ( pendent ); 
        var degrees     = 180 * angle / Math.PI;

        // angle to rotate arrow triangle to be perpendicular to line
        var nearDegrees = (Direction=="RT" || Direction=="LB")?-45:45;
        var nearAngle   = Math.PI * nearDegrees / 180;
        var fixedAngle  = this._getCorrectedAngle( nearAngle, angle );

        // cap styles
        var capWidth    = sW * wF;
        var capHeight   = sW * hF;
        ctx.fillStyle   = st;  

        // translate position cap to end of line (-1/+1 to avoid transparent gaps)
        // also rotate if needed
        // then create cap path
        if(xDirection=="L" && yDirection=="T") {
            ctx.translate( -w/2 +1, -h/2 +1);

            ctx.rotate ( fixedAngle );

            ctx.beginPath();
            ctx.moveTo(0, 0);
                ctx.lineTo(capWidth, -capWidth);
                ctx.lineTo(-capHeight, -capHeight);
                ctx.lineTo(-capWidth, capWidth);
        }
        else if(xDirection=="R" && yDirection=="T") {
            ctx.translate( w/2 -1, -h/2 +1);

            ctx.rotate ( fixedAngle );

            ctx.beginPath();
            ctx.moveTo(0, 0);
                ctx.lineTo(-capWidth, -capWidth);
                ctx.lineTo(capHeight, -capHeight);
                ctx.lineTo(capWidth, capWidth);
        }
        else if(xDirection=="R" && yDirection=="B") {
            ctx.translate( w/2 -1, h/2 -1);

            ctx.rotate ( fixedAngle );

            ctx.beginPath();
            ctx.moveTo(0, 0);
                ctx.lineTo(capWidth, -capWidth);
                ctx.lineTo(capHeight, capHeight);
                ctx.lineTo(-capWidth, capWidth);
        }
        else if(xDirection=="L" && yDirection=="B") {
            ctx.translate( -w/2 +1, h/2 -1);

            ctx.rotate ( fixedAngle );

            ctx.beginPath();
            ctx.moveTo(0, 0);
                ctx.lineTo(-capWidth, -capWidth);
                ctx.lineTo(-capHeight, capHeight);
                ctx.lineTo(capWidth, capWidth);
        }
        else if(xDirection=="L" && yDirection=="-") {
            ctx.translate( -w/2 +1, -h/2 +1);

            ctx.beginPath();
            ctx.moveTo(0, 0);
                ctx.lineTo(0, -capWidth);
                ctx.lineTo(-capHeight, 0);
                ctx.lineTo(0, capWidth);
        }
        else if(xDirection=="R" && yDirection=="-") {
            ctx.translate( w/2 -1, -h/2 +1);

            ctx.beginPath();
            ctx.moveTo(0, 0);
                ctx.lineTo(0, -capWidth);
                ctx.lineTo(capHeight, 0);
                ctx.lineTo(0, capWidth);
        }
        else if(xDirection=="|" && yDirection=="T") {
            ctx.translate( 0, -h/2 +1);

            ctx.beginPath();
            ctx.moveTo(0, 0);
                ctx.lineTo(-capWidth, 0);
                ctx.lineTo(0, -capHeight);
                ctx.lineTo(capWidth, 0);
        }
        else if(xDirection=="|" && yDirection=="B") {
            ctx.translate( 0, h/2 -1);

            ctx.beginPath();
            ctx.moveTo(0, 0);  
                ctx.lineTo(-capWidth, 0);
                ctx.lineTo(0, capHeight);
                ctx.lineTo(capWidth, 0);
        }
            
        // close any path
        ctx.closePath();            

        // add arrow cap
        this._renderFill(ctx);
    },

    toObject: function(propertiesToInclude) {
        // console.log("Arrow.toObject");
        return fabric.util.object.extend(this.callSuper('toObject', propertiesToInclude), {
            x1: this.get('x1'),
            y1: this.get('y1'),
            x2: this.get('x2'),
            y2: this.get('y2'),
            wFactor: this.get('wFactor'),
            hFactor: this.get('hFactor')
        });
    }
});

fabric.Arrow.fromObject = function(object) {
    // console.log("Arrow.fromObject", object);
    var points = [object.x1, object.y1, object.x2, object.y2];
    return new fabric.Arrow(points, object);
};