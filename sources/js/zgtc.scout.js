
/*!
------------------------------------------------------------------------------------------------------
                                                                                            ZGTC OBJECT
  ----------------------------------------------------------------------------------------------------- */
var ZGTC = {};

/*!
------------------------------------------------------------------------------------------------------
                                                                                                 SCOUT
 ----------------------------------------------------------------------------------------------------- */
/*!
    Scout decides if we load the rest of JS (test document.getElementById)
    In old IE we load old versions of jQuery, fabricJS, …
*/

ZGTC.Scout = {
    init : function( what ){
        if(document.getElementById === "undefined") {
            alert("OLD BROWSER");
            return;
        }
        document.documentElement.className = document.documentElement.className.replace(/(\s|^)no-js(\s|$)/, '$1' + 'js' + '$2');
        var base    = 'public/js/',
            checks;

        if(!!(window.attachEvent && !window.addEventListener)) checks = "oldie";
        else checks = "goodie";

        var script = document.createElement('script');
            script.src = base + checks + what + '.min.js';
            console.log( script.src );
            script.async = false;
        document.getElementsByTagName("head")[0].appendChild(script);
    }
}

function initScout () {
    ZGTC.Scout.init(".navigator");
}

if (window.addEventListener) {
    window.addEventListener("load", initScout);
} else if (window.attachEvent) {
    window.attachEvent("onload", initScout);
} else {
    setTimeout(initScout, 0);
}
