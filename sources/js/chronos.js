/*!
CHRONOS: uncoupled undo/redo manager for web apps
v0.1.0, Sergi Meseguer @zigotica
https://github.com/zigotica/chronosJS/

Depends on:
jQuery http://jquery.com/
jQuery Tiny Pub/Sub https://github.com/cowboy/jquery-tiny-pubsub
*/

Chronos = {
    states  : [],
    current : -1,
    from    : -1,
    undoElm : "#undo",
    redoElm : "#redo",

    init : function(){
        $.subscribe("save.chronostate", this.save);
        $.subscribe("discard.chronostate", this.discard);
        $.subscribe("reset.chronostate", this.reset);
    },

    reset : function(){
        Chronos.states  = [];
        Chronos.from    = -1;
        Chronos.current = -1;

        // update button states
        Chronos.updateButtons();
    },

    save : function( e, what ){
        // e not used, needed as per "jQuery Tiny Pub/Sub" requirement
        var ns      = Chronos,
            title   = what.title,
            content = what.content;

        // update previous state from current data
        ns.from = ns.current;

        console.log("PUBSUB SYSTEM - SAVE NEW CHRONO STATE", title);
        if(ns.current > -1 && ns.current < ns.states.length - 1) {
            // discard rest since we are rewriting the history from the middle
            ns.states = ns.states.slice( 0, ns.current + 1);
        }
        // now we can add at the end, always correct position
        ns.current ++;
        ns.states.push( {"id": ns.current, "title": title, "content": content });

        // update button states
        ns.updateButtons();
    },

    discard : function(e, what){
        // e not used, needed as per "jQuery Tiny Pub/Sub" requirement
        var ns      = Chronos,
            title   = what.title,
            content = what.content;

        // update previous state from current data
        ns.from = ns.current;

        console.log("PUBSUB SYSTEM - DISCARD LAST CHRONO STATE", title);
        ns.states[ns.current] = {"id": ns.current, "title": title, "content": content };
    },

    undo : function(){
        // update previous state from current data
        this.from = this.current;

        // enabled/disable logic at updateButtons()
        this.current --;

        // rewrite from states array
        this.rewrite();
    },

    redo : function(){
        // update previous state from current data
        this.from = this.current;

        // enabled/disable logic at updateButtons()
        this.current ++;

        // rewrite from states array
        this.rewrite();
    },

    rewrite : function(){
        $.publish("rewrite.chronostate", this.states[this.current]);

        // update button states
        this.updateButtons();
    },

    updateButtons : function(){
        // undo
        if(this.current > 0) {
            $(this.undoElm).prop("disabled", false).attr("title", this.states[this.current-1]["title"]);
        }
        else {
            $(this.undoElm).prop("disabled", true).attr("title","");
        }
        // redo
        if(this.current > -1 && this.current < this.states.length - 1) {
            $(this.redoElm).prop("disabled", false).attr("title", this.states[this.current+1]["title"]);
        }
        else {
            $(this.redoElm).prop("disabled", true).attr("title","");
        }
    }
};
Chronos.init();