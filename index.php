<?php
// remove all temporary images older than 1h
require 'download/remove.php';
// include customizations
require '___vars.theme.php';
require '___vars.localization.'. $LANG .'.php';

// include security check (put the client API key into $CLIENTKEY at ___vars.theme.php)
if ($CLIENTKEY !== '' && $CLIENTKEY !== 'the_API_key')
    if(file_get_contents('http://ws.ec-europe.com/internal/index?referer='. $_SERVER['HTTP_REFERER'] .'&key='. $CLIENTKEY) !== '1') exit('403 Access Forbidden');

?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $WEBTITLE; ?></title>
    <meta charset="utf-8">
    <!--[if lt IE 9]>
        <script src="public/js/ie/html5shiv-printshiv.min.js"></script>
        <script src="public/js/ie/css3.mediaqueries.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="public/css/style.css">
    <style>
    html,body { font-family: <?php echo $APPFONT; ?>; }
    #toplinks { background-color: <?php echo $HEADBGCOLOR; ?>; border-bottom: 1px solid <?php echo $HEADBRCOLOR; ?>;}
    #toplinks a { color: <?php echo $HEADLKCOLOR; ?>;}
    #toplinks > a { border-left: 1px solid <?php echo $HEADLKBRCOL; ?>;}
    #term { color: <?php echo $INPCOLTXT; ?>; }
    #searchbox { border-bottom: 1px solid <?php echo $INPBRCOLOR; ?>; }
    #search button .icon { color: <?php echo $INPCOLTXT; ?>; }
    #search button:disabled .icon { color: <?php echo $INPCOLPLC; ?>; }
    #term::-webkit-input-placeholder { color: <?php echo $INPCOLPLC; ?>; }
    #term:-ms-input-placeholder { color: <?php echo $INPCOLPLC; ?>; }
    #clickable { color: <?php echo $INPINFOCL; ?>; }
    #main { background-color: <?php echo $LEFTBGCOL; ?>; }
    .filter { color: <?php echo $LINKCOLOR; ?>;  background-color: <?php echo $LINKBGCOL; ?>; border-bottom: 1px solid <?php echo $LINKBRCOLTP; ?>; }
    .filter.active,.filter:hover { color: <?php echo $LINKCOLORAH; ?>;  background-color: <?php echo $LINKBGCOLAH; ?>; }
    .filter.filter-chapter { color: <?php echo $CHPTCOLOR; ?>;  background-color: <?php echo $CHPTBGCOL; ?>; }
    .filter.filter-chapter.active, .filter.filter-chapter:hover { color: <?php echo $CHPTCOLORAH; ?>;  background-color: <?php echo $CHPTBGCOLAH; ?>; }
    #content { background-color: <?php echo $THUMBSBGCOL; ?>; }
    .fulltext::-webkit-scrollbar-track { background-color: <?php echo $SRCBGCOL; ?>; }
    .fulltext::-webkit-scrollbar-thumb { background-color: <?php echo $SRCHNCOL; ?>; }
    .fulltext::-webkit-scrollbar-thumb:hover { background-color: <?php echo $SRCACCOL; ?>; }    #thumbsboxfilter { color: <?php echo $FILCOLINF; ?>; }
    figcaption .thumbinfoikon { background-color: white; color: <?php echo $OVERCOLINF; ?>; }
    figcaption.showing .thumbinfoikon, figcaption .thumbinfoikon:hover { background-color: <?php echo $OVERCOLINF; ?>; color: white; }
    #modalcont { color: <?php echo $MODCOLOR; ?>;  background-color: <?php echo $MODBGCOL; ?>; }
    #modalhtml .close-button { color: <?php echo $MODCLCOL; ?>;  }
    #modalcont::-webkit-scrollbar-track { background-color: <?php echo $SRCBGCOL; ?>; }
    #modalcont::-webkit-scrollbar-thumb { background-color: <?php echo $SRCHNCOL; ?>; }
    #modalcont::-webkit-scrollbar-thumb:hover { background-color: <?php echo $SRCACCOL; ?>; }
    #editor .actions { background-color: <?php echo $ACTBARBG; ?>; }
    #editor .actions---sec, #subactions { background-color: <?php echo $SECACTBG; ?>; }
    #editor .actions .action { color: <?php echo $ACTCOLOR; ?>;  background-color: <?php echo $ACTBGCOL; ?>; }
    #editor .actions .action:disabled, .action:disabled .icon { color: <?php echo $ACTDISCOLOR; ?>!important; background-color: <?php echo $ACTDISBG; ?>!important; }
    #editor .actions .active-button, #editor .actions .action:hover { color: <?php echo $ACTCOLORAH; ?>;  background-color: <?php echo $ACTBGCOLAH; ?>; }
    .action .icon { color: <?php echo $ACTCOLOR; ?>; }
    .action.active-button .icon, #editor .actions .action:hover .icon { color: <?php echo $ACTCOLORAH; ?>; }
    #subactions .fontselector { color: <?php echo $_MENUBGC; ?>; }
    #subactions .fontselector.selected { color: <?php echo $_BOOKAHC; ?>; }
    .noUi-connect { background-color: <?php echo $SLIDERBGCOL; ?>; }
    </style>
    <script src="//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
    <script>
    WebFontBuffer = [];
    PALETTE = <?php echo $PALETTE; ?>;
    PALETTEMAIN = "<?php echo $PALETTEMAIN; ?>";
    USERID = "<?php echo $USERID; ?>";
    SITEID = "<?php echo $SITEID; ?>";
    EditorMessages = {
        // actions
         "filtby": "<?php echo $filtrby; ?>"
        ,"init"  : "<?php echo $init; ?>"
        ,"new"   : "<?php echo $new; ?>"
        ,"mod"   : "<?php echo $mod; ?>"
        ,"del"   : "<?php echo $del; ?>"
        ,"prop"  : "<?php echo $prop; ?>"
        ,"cancel": "<?php echo $cancel; ?>"
        ,"ok"    : "<?php echo $ok; ?>"
        // objects
        ,"text"  : "<?php echo strtolower($Text); ?>"
        ,"addtxt": "<?php echo $addtxt; ?>"
        ,"addok" : "<?php echo $addok; ?>"
        ,"line"  : "<?php echo strtolower($Line); ?>"
        ,"arrow" : "<?php echo strtolower($Arrow); ?>"
        ,"rect"  : "<?php echo strtolower($Rect); ?>"
        ,"circle": "<?php echo strtolower($Circle); ?>"
        ,"pencil": "<?php echo strtolower($Pencil); ?>"
        ,"crop"  : "<?php echo strtolower($Crop); ?>"
        ,"filter": "<?php echo strtolower($Filter); ?>"
        // props
        ,"color" : "<?php echo strtolower($Color); ?>"
        ,"brightness": "<?php echo strtolower($BR); ?>"
    };
    WebFontConfig = {
        <?php echo $GOOGLEFONT; ?>
        <?php echo $CUSTOMFONT; ?>
        fontactive: function(familyName, fvd) {
            WebFontBuffer.push( familyName );
        },
        fontinactive: function(familyName, fvd) { }
    };
    WebFont.load(WebFontConfig);
    </script>
    <!--[if lt IE 8]>
        <link rel="stylesheet" href="public/js/ie/ie7icons.min.js">
    <![endif]-->
    <!--[if lt IE 9]>
        <link rel="stylesheet" href="public/css/ie/editor.ie8.css">
    <![endif]-->
    <style media="print">
        @page {
            margin: 2cm;
        }
        #toplinks, #main, .actions, #subactions { display: none; width: 0; max-width: 0; height:0; max-height: 0;  }
        #editarea, .canvas-container, canvas {
            display: block;
            width: 100%;
            height: auto;
            border: 0;
        }
    </style>
</head>

<body class="mode-navigator">
<?php if ($USECOOKIEBANNER === true) { // turn it on changing 'false' to 'true' into $USECOOKIEBANNER at ___vars.theme.php ?>
<!-- Cookies top banner start -->
<style>
.divoverlay {
    background-color: #EDF4FB;
    box-shadow: 0 0 10px #AAA;
    color: #000;
    display: none;
    font-family: Arial;
    font-size: 9pt;
    margin-bottom: 10px;
    overflow: hidden;
    text-align: center;
    width: 100%;
    padding: 12px 0 12px;
    position: relative;
    z-index: 999;
}
.divoverlay p {
    margin: 0;
}
.divsuboverlay {
    line-height: 16px;
    width: 991px;
    position: relative;
    margin: 0 auto;
    padding: 0 20px;
}
.closeicon {
    top: -3px;
    position: absolute;
    right: 0;
    width: 20px;
    height: 20px;
    border-radius: 3px;
    background-color: #d7e7f7;
    color: #666;
    text-align: center;
    line-height: 20px;
    font-size: 20px;
}
a.closeicon {
    text-decoration: none
}
.pannelHead .rtxt_title {
    display: none;
}
#cookie-notice-link {
    position: absolute;
    bottom: 0;
    left: 20px;
    z-index: 9999;
    font-family: Arial;
    font-size: 13px;
    background-color: #fff;
    padding: 2px 5px;
    border-radius: 3px;
}
#cookie-notice-link a {
    text-decoration: none
}
body {
    position: relative;
    height: 100%
}
</style>
<div class="divoverlay" id="divoverlayid">
    <div class="divsuboverlay" id="divsuboverlay_0">
        <div class="pannelHead" id="pannelHead_0">
            <div>
                <a href="#" class="closeicon" onclick="setCookie('ec_close_cookiepanel', 'true', 365);document.getElementById('divoverlayid').style.display = 'none';">×</a>
            </div>
            <div class="rtxt">
                <p class="rtxt_title"><?php echo $cookie_banner_title ?></p>
                <p><?php echo $cookie_message_line_1 ?></p>
                <p><?php echo $cookie_message_line_2 ?></p>
            </div>
        </div>
    </div>
</div>
<script>
function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) +
        ((exdays == null) ? "" : ("; expires=" + exdate.toUTCString()));
    document.cookie = c_name + "=" + c_value;
}
function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }
    return false;
}
if (getCookie('ec_close_cookiepanel') === false) {
    document.getElementById('divoverlayid').style.display = 'block';
}
</script>
<!-- Cookies top banner end -->
<?php } ?>
    <script>
    var J = <?php require '___data.json.php'; ?>;
    </script>
    <script id="thumb_template_minimal" type="text/mustache">
        <% for (var i = 0; i < items.length; i++) { %>
            <% var item = items[i]; %>
            <figure class="thumb">
                <img src="public/thumbs/<%=item.id%>.png">
                <% if(item.itembody){ %>
                <figcaption>
                    <div class="fulltext"><%=item.itembody%></div>
                    <p><?php echo $fulltext; ?></p>
                </figcaption>
                <% } %>
            </figure>
        <% } %>
    </script>
    <script id="thumb_template" type="text/mustache">
        <% for (var i = 0; i < items.length; i++) { %>
            <% var item = items[i]; %>
                <figure class="thumb editable">
                    <a onclick="ZGTC.Editor.init(<%=item.id%>, '<%=item.itemtitle%>', '<%=item.logofile%>', '<%=item.logolink%>')" title="<%=item.itemtitle%>">
                        <img id="thumbs_id_<%=item.id%>" src="public/thumbs/<%=item.id%>.png">
                    </a>
                    <% if(item.itembody){ %>
                    <figcaption>
                        <div class="fulltext"><%=item.itembody%></div>
                        <p><?php echo $fulltext; ?></p>
                        <a href="#" class="thumbinfoikon icon icon-info"></a>
                    </figcaption>
                    <% } %>
                </figure>
        <% } %>
    </script>

    <div id="toplinks">
        <span id="logo"><?php
            if($LOGOLINK == "") echo " <img src='public/logos/$LOGOFILE' alt='logo'>";
            else echo "<a href='$LOGOLINK' target='_blank'><img src='public/logos/$LOGOFILE' alt='logo'></a>";
        ?></span>
        <a id="back" href="#" onclick="ZGTC.Editor.end()"><?php echo $home; ?></a>
        <a href="___page.help.php" class="modal"><?php echo $help; ?></a>
        <a href="___page.tos.php" class="modal"><?php echo $tos; ?></a>
        <?php if ($USECOOKIEBANNER === true) {
            echo '<a href="cookie-notice.'. $LANG .'.php" target="cookie_notice">'. $cookies .'</a>';
        } ?>
    </div>

    <div id="main">
        <div id="searchbox">
            <form id="search" style="display:none">
                <input id="term" type="text" placeholder="<?php echo $search; ?>">
                <button type="submit" disabled><span class="icon icon-search"></span></button>
            </form>
            <span id="clickable"><?php echo $ready; ?></span>
        </div>
        <div id="menu"></div>
        <div id="content">
            <div id="thumbsbox"><span id="thumbsboxfilter"></span><div id="thumbs"></div></div>
        </div>
    </div>

    <div id="editorbox">
        <div id="editorloading"></div>
        <div id="editor">
            <div id="editarea"></div>
            <div class="actions actions---main">
                <button class="action" onclick="ZGTC.Editor.setMode(this,'i-text')" title="<?php echo $Text; ?>"><span class="icon icon-text"></span></button>
                <button class="action" onclick="ZGTC.Editor.setMode(this,'line')" title="<?php echo $Line; ?>"><span class="icon icon-line"></span></button>
                <button class="action" onclick="ZGTC.Editor.setMode(this,'arrow')" title="<?php echo $Arrow; ?>"><span class="icon icon-arrow"></span></button>
                <button class="action" onclick="ZGTC.Editor.setMode(this,'path')" title="<?php echo $Pencil; ?>"><span class="icon icon-free"></span></button>
                <button class="action" onclick="ZGTC.Editor.setMode(this,'rect')" title="<?php echo $Rect; ?>"><span class="icon icon-rect"></span></button>
                <button class="action" onclick="ZGTC.Editor.setMode(this,'circle')" title="<?php echo $Circle; ?>"><span class="icon icon-circle"></span></button>
                <button id="filtreme" class="action" onclick="ZGTC.Editor.setMode(this,'imgfilters')" title="<?php echo $Filter; ?>"><span class="icon icon-filters"></span></button>
                <button id="cropme" class="action" onclick="ZGTC.Editor.setMode(this,'crop')" title="<?php echo $Crop; ?>"><span class="icon icon-crop"></span></button>
            </div>
            <div class="actions actions---sec">
                <div class="actiongroup">
                    <button class="action" onclick="ZGTC.Editor.undo()" id="undo" title="<?php echo $Undo; ?>" disabled><span class="icon icon-undo"></span></button>
                    <button class="action" onclick="ZGTC.Editor.redo()" id="redo" title="<?php echo $Redo; ?>" disabled><span class="icon icon-redo"></span></button>
                </div>
                <div class="actiongroup">
                    <a class="action" href="#" onclick="ZGTC.Editor.prepareDownload()" id="downloadme" download="TITULO.png" target="_blank" title="<?php echo $Save; ?>"><span class="icon icon-download"></span></a>
                    <button class="action" onclick="ZGTC.Editor.print()" title="<?php echo $Print; ?>"><span class="icon icon-print"></span></button>
                </div>
            </div>
            <div id="subactions">
                <div id="text" class="subactions-type">
                    <div class="subaction range">
                        <label class="label" for="fs"><?php echo $FS; ?></label>
                        <p id="fontfamily">
                          <a href="#" class="fontselector selected" data-font="<?php echo $EDITORFONT; ?>" style="font-family:<?php echo $EDITORFONT; ?>"><?php echo $EDITORFONT; ?></a>
                        </p>
                        <div id="fs" class="slider" data-range-min="<?php echo $FONTMIN; ?>" data-range-max="<?php echo $FONTMAX; ?>" data-range-step="<?php echo $FONTINC; ?>" data-range-value="<?php echo $FONTSZ; ?>"></div>
                    </div>
                    <div class="subaction">
                        <label class="label" for="textcolor"><?php echo $Color; ?></label> <input id="textcolor" name="textcolor">
                    </div>
                </div>
                <div id="line" class="subactions-type">
                    <div class="subaction  range">
                        <label class="label" for="swidth"><?php echo $LW; ?></label>
                        <div id="width" class="slider" data-range-min="<?php echo $LINEMIN; ?>" data-range-max="<?php echo $LINEMAX; ?>" data-range-step="<?php echo $LINEINC; ?>" data-range-value="<?php echo $LINESZ; ?>"></div>
                    </div>
                    <div class="subaction">
                        <label class="label" for="linecolor"><?php echo $Color; ?></label> <input id="linecolor" name="linecolor">
                    </div>
                </div>
                <div id="area" class="subactions-type">
                    <div class="subaction  range">
                        <label class="label" for="swidth"><?php echo $SW; ?></label>
                        <div id="swidth" class="slider" data-range-min="<?php echo $AREAMIN; ?>" data-range-max="<?php echo $AREAMAX; ?>" data-range-step="<?php echo $AREAINC; ?>" data-range-value="<?php echo $AREASZ; ?>"></div>
                    </div>
                    <div class="subaction">
                        <label class="label" for="strokecolor"><?php echo $SC; ?></label> <input id="strokecolor" name="strokecolor">
                    </div>
                    <div class="subaction">
                        <label class="label" for="fillcolor"><?php echo $FC; ?></label> <input id="fillcolor" name="fillcolor">
                    </div>
                </div>
                <div id="imgfilters" class="subactions-type">
                    <div id="brightfilter" class="subaction range">
                        <label class="label" for="Brightness"><?php echo $BR; ?></label>
                        <div id="Brightness" class="slider" data-range-min="-255" data-range-max="255" data-range-step="5" data-range-value="0"></div>
                    </div>

                    <div id="contrastfilter" class="subaction range">
                        <label class="label" for="Contrast"><?php echo $CN; ?></label>
                        <div id="Contrast" class="slider" data-range-min="-1" data-range-max="5" data-range-step="1" data-range-value="0"></div>
                    </div>

                    <div id="colorfilter" class="subaction">
                        <label class="label" for="Blend"><?php echo $Color; ?></label>
                        <input id="Blend" name="Blend">
                    </div>

                    <div id="bwfilter" class="subaction">
                        <label class="label" for="BW"><?php echo $BW; ?></label>
                        <input id="BW" name="BW" type="checkbox">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="footercode"><?php echo $FOOTERCODE; ?></div>

    <!-- Piwik -->
    <script type="text/javascript">
      var _paq = _paq || [];
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
        var u= "<?php echo $SITEID; ?>";
        _paq.push(['setTrackerUrl', u+'piwik.php']);
        _paq.push(['setSiteId', <?php echo $USERID; ?>]);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
      })();
    </script>
    <noscript><p><img src="<?php echo $SITEID; ?>/piwik.php?idsite=<?php echo $USERID; ?>" style="border:0;" alt="" /></p></noscript>
    <!-- End Piwik Code -->
    <script src="public/js/loader.min.js"></script>
<?php 
include "login.php";
?>
</body>
</html>
