<?php

// ------------------------------------------------------- toplinks / search
$home    = "Pagina iniziale";
$help    = "Help";
$tos     = "Licenza";
$cookies = "Cookies";
$search  = "Cerca";
$ready   = "Clicca sull'immagine";
$filtrby = "Filtra risultati";
$fulltext= "Testo integrale";
// ------------------------------------------------------- login
$strLogin= "Pagina di accesso";
$strUser = "Nome utente";
$strPass = "Password";
$strError= "ID o password non validi";
// ------------------------------------------------------- actions
$Text    = "Testo";
$addtxt  = "Aggiungi testo";
$addok   = " (Da modificare in seguito)";
$Line    = "Linea";
$Arrow   = "Freccia";
$Pencil  = "Matita";
$Crop    = "Taglierina";
$Rect    = "Rettangolo";
$Circle  = "Cerchio";
$Filter  = "Filtri";
$Undo    = "Annulla";
$Redo    = "Ripeti";
$Save    = "Download";
$Print   = "Stampa";
$FS      = "Corpo";
$Color   = "Colore";
$LW      = "Spessore";
$SW      = "Spessore";
$SC      = "Bordo";
$FC      = "Riempimento";
$BR      = "Luminosità";
$CN      = "Contrasto";
$BW      = "Bianco e nero";
// ------------------------------------------------------- undo/redo, color pickers
$init    = "Nuova scena";
$new     = "Oggetto inserito: ";
$mod     = "Oggetto modificato: ";
$del     = "Oggetto cancellato: ";
$prop    = "Modifiche effettuate: ";
$cancel  = "cancella";
$ok      = "OK";
// ------------------------------------------------------- cookie banner (remember to escape eventual ' chars like \' into messages)
$cookie_banner_title = 'Informazione su cookie';
$cookie_message_line_1 = 'Questo sito web utilizza i cookie per aiutarci a offrirvi la migliore esperienza quando visitate il nostro sito web. Continuando ad utilizzare questo sito web, voi acconsentite al nostro uso di tali cookies.';
$cookie_message_line_2 = 'Saperne più su come utilizziamo i cookie e come gestirli leggendo la nostra <a class="cookie-notice" href="cookie-notice.'. $LANG .'.php" target="cookie_notice">cookie preavviso</a>.';
