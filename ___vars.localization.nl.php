﻿<?php

// ------------------------------------------------------- toplinks / search
$home    = "Home";
$help    = "Help";
$tos     = "Voorwaarden";
$cookies = "Cookies";
$search  = "Zoek";
$ready   = "Klik op het plaatje voor bewerken";
$filtrby = "Filter zoekopdracht: ";
$fulltext= "Full text";
// ------------------------------------------------------- login
$strLogin= "Login Pagina";
$strUser = "Gebruikersnaam";
$strPass = "Wachtwoord";
$strError= "ID of wachtwoord ongeldig";
// ------------------------------------------------------- actions
$Text    = "Tekst";
$addtxt  = "Tekst toevoegen";
$addok   = " (kan achteraf aangepast worden)";
$Line    = "Lijnsegment";
$Arrow   = "Pijl";
$Pencil  = "Potlood";
$Crop    = "Afknippen";
$Rect    = "Rechthoek";
$Circle  = "Cirkel";
$Filter  = "Filters";
$Undo    = "Ongedaan maken";
$Redo    = "Opnieuw";
$Save    = "Opslaan";
$Print   = "Print";
$FS      = "Tekengrootte";
$Color   = "Kleur";
$LW      = "Lijndikte";
$SW      = "Randdikte";
$SC      = "Randkleur";
$FC      = "Vulkleur";
$BR      = "Helderheid";
$CN      = "Contrast";
$BW      = "Grijstint";
// ------------------------------------------------------- undo/redo, color pickers
$init    = "Nieuw";
$new     = "Geplaatst object: ";
$mod     = "Aangepast object: ";
$del     = "Verwijderd object: ";
$prop    = "Bewerkte eigenschappen: ";
$cancel  = "Annuleer";
$ok      = "OK";
// ------------------------------------------------------- cookie banner (remember to escape eventual ' chars like \' into messages)
$cookie_banner_title = 'Cookies Informatie';
$cookie_message_line_1 = 'Deze website maakt gebruik van cookies om ons te helpen geven u de beste ervaring wanneer u onze website bezoekt. Door te blijven deze website, gaat u akkoord met ons gebruik van deze cookies.';
$cookie_message_line_2 = 'Ontdek meer over hoe we gebruik van cookies en hoe ze te beheren door het lezen van onze <a class="cookie-notice" href="cookie-notice.'. $LANG .'.php" target="cookie_notice">cookie-bericht</a>.';
