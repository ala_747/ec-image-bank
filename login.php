<?php 
if (is_file("public/password.csv") && !$_COOKIE['logged']) {
?>
<style>
#login-overlay {
position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background-color: #000;
filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=50);
opacity: 0.5;
z-index: 1000001
}
#login-overlay {
	filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=100);
	opacity: 1;
	background:#fff
}
.float-left {float:left}
img.float-left {
z-index: 1000004;
top:0;
left:0;
position:absolute;
}

#login {
	z-index: 1000004;
	width: 400px;
	height:400px;
	position: fixed;
	top: 50%;
	left: 600px;
	text-align: center;
	margin-top: -200px;
	background: #fff;
	border-radius: 10px;
	padding: 20px;
	text-align: left;
}

#login h1{
	font-size:55px;
	font-weight:bold;
	color:<?php echo $_MAINCOL; ?>;
}

#login h2{
	font-size:35px;
	font-weight:normal;
	color:#666;
}
#login h3{
	font-size:20px;
	font-weight:bold;
	color:<?php echo $_MAINCOL; ?>;
}


#login label, #login p {
	display: block;
	position: relative;
	line-height: 30px;
}
#login label input {
	position: absolute;
	top:2px;
	left: 100px
}
#login-error {
	display:none;
	color: #c00;
	font-weight: bold
}
</style>
<div id="login-overlay"></div>
<img src="public/login-image.jpg" class="float-left">
<form id="login" class="float-left">
	<h1><?php echo $WEBTITLE ?></h1>
	<h2>Image Bank</h2>
	<h3><?php echo $strLogin; ?></h3>

	<label><?php echo $strUser ?> <input type="text" name="user" id="login-user" /></label>
	<label><?php echo $strPass ?> <input type="password" name="password" id="login-password" /></label>
	<p id="login-error"></p>
	<label><input type="submit" value="<?php echo $ok ?>" id="login-submit" /></label>
	<br><br><img src="public/logos.jpg">
</form>

<script>
var loginData = "<?php echo str_replace(array("\r", "\n"), '\\\\', file_get_contents("public/password.csv")); ?>";
var loginError = "<?php echo str_replace('\'', '\\\'', $strError); ?>";
</script>
<?php
}
?>