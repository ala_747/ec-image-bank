<?php

// ------------------------------------------------------- toplinks / search
$home    = "Home";
$help    = "Help";
$tos     = "User terms";
$cookies = "Cookies";
$search  = "Search";
$ready   = "Click on an image to open the editor";
$filtrby = "Filter results by: ";
$fulltext= "Full text";
// ------------------------------------------------------- login
$strLogin= "Login Page";
$strUser = "User";
$strPass = "Password";
$strError= "Wrong user or password";
// ------------------------------------------------------- actions
$Text    = "Text";
$addtxt  = "Add a line of text";
$addok   = " (can be modified afterwards)";
$Line    = "Line";
$Arrow   = "Arrow";
$Pencil  = "Pencil";
$Crop    = "Crop";
$Rect    = "Rectangle";
$Circle  = "Circle";
$Filter  = "Filters";
$Undo    = "Undo";
$Redo    = "Redo";
$Save    = "Download";
$Print   = "Print";
$FS      = "Font size";
$Color   = "Color";
$LW      = "Line width";
$SW      = "Border width";
$SC      = "Border color";
$FC      = "Filling color";
$BR      = "Luminosity";
$CN      = "Contrast";
$BW      = "Black and white";
// ------------------------------------------------------- undo/redo, color pickers
$init    = "new canvas";
$new     = "object inserted: ";
$mod     = "object modified: ";
$del     = "object deleted: ";
$prop    = "feature modified: ";
$cancel  = "cancel";
$ok      = "OK";
// ------------------------------------------------------- cookie banner (remember to escape eventual ' chars like \' into messages)
$cookie_banner_title = 'Cookies Information';
$cookie_message_line_1 = 'This website uses cookies to help us give you the best experience when you visit our website. By continuing to use this website, you consent to our use of these cookies.';
$cookie_message_line_2 = 'Find out more about how we use cookies and how to manage them by reading our <a class="cookie-notice" href="cookie-notice.'. $LANG .'.php" target="cookie_notice">cookie notice</a>.';
