<?php

$DATA = $GLOBALS["HTTP_RAW_POST_DATA"];

if (isset($DATA)) {
    // convert JSON OBJECT INTO SEPARATE FIELDS
    $json   = json_decode($DATA,true);
    $image  = $json['img'];
    $name   = $json['name'];

    // Remove the headers (data:,) part.
    // A real application should use them according to needs such as to check image type
    $filteredData=substr($image, strpos($image, ",")+1);

    // Need to decode before saving since the data we received is already base64 encoded
    $unencodedData=base64_decode($filteredData);

    // Save file. 
    $fp = fopen( $name.'.png', 'wb');
    fwrite( $fp, $unencodedData);
    fclose( $fp );
}

?>