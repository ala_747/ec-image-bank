<?php

// ------------------------------------------------------- toplinks / search
$home    = "Inicio";
$help    = "Ayuda";
$tos     = "Términos de uso";
$cookies = "Cookies";
$search  = "Buscar";
$ready   = "Click en una imagen para abrir el editor";
$filtrby = "Resultados filtrados por: ";
$fulltext= "Texto Completo";
// ------------------------------------------------------- login
$strLogin= "Identifíquese";
$strUser = "Usuario";
$strPass = "Contraseña";
$strError= "Usuario o contraseña inválidos";
// ------------------------------------------------------- actions
$Text    = "Texto";
$addtxt  = "Añade tu línea de texto";
$addok   = " (se puede modificar más adelante)";
$Line    = "Línea";
$Arrow   = "Flecha";
$Pencil  = "Pincel";
$Crop    = "Recortar";
$Rect    = "Rectángulo";
$Circle  = "Círculo";
$Filter  = "Filtros";
$Undo    = "Deshacer";
$Redo    = "Rehacer";
$Save    = "Descargar";
$Print   = "Imprimir";
$FS      = "Tamaño de fuente";
$Color   = "Color";
$LW      = "Ancho de línea";
$SW      = "Ancho de contorno";
$SC      = "Color de contorno";
$FC      = "Color de relleno";
$BR      = "Luminosidad";
$CN      = "Contraste";
$BW      = "Blanco y negro";
// ------------------------------------------------------- undo/redo, color pickers
$init    = "lienzo nuevo";
$new     = "objeto insertado: ";
$mod     = "objeto modificado: ";
$del     = "objeto eliminado: ";
$prop    = "propiedad modificada: ";
$cancel  = "cancelar";
$ok      = "OK";
// ------------------------------------------------------- cookie banner (remember to escape eventual ' chars like \' into messages)
$cookie_banner_title = 'Información sobre cookies';
$cookie_message_line_1 = 'Este sitio web usa cookies para ayudarnos a brindarle la mejor experiencia de uso cuando nos visita. Si continua usándolo, está aceptando el uso de dichas cookies.';
$cookie_message_line_2 = 'Sepa más sobre cómo usamos las cookies y cómo gestionarlas leyendo nuestra <a class="cookie-notice" href="cookie-notice.'. $LANG .'.php" target="cookie_notice">información sobre cookies</a>.';
