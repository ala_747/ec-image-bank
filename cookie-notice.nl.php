<?php 
require '___vars.theme.php';
require '___vars.localization.'. $LANG .'.php';
?>
<!DOCTYPE html>
<html lang="<?php echo $LANG; ?>">

<head>
    <meta charset="UTF-8">
    <title><?php echo $cookie_banner_title ?></title>
    <style>
        .parsys {
            font: 100%/1.4 Helvetica Neue, helvetica, arial, sans-serif;
            margin: 0 20%;
        }
    </style>
</head>

<body>
<!-- Cookies top banner start -->
<style>
.divoverlay {
    background-color: #EDF4FB;
    box-shadow: 0 0 10px #AAA;
    color: #000;
    display: none;
    font-family: Arial;
    font-size: 9pt;
    margin-bottom: 10px;
    overflow: hidden;
    text-align: center;
    width: 100%;
    padding: 12px 0 12px;
    position: relative;
    z-index: 999;
}
.divoverlay p {
    margin: 0;
}
.divsuboverlay {
    line-height: 16px;
    width: 991px;
    position: relative;
    margin: 0 auto;
    padding: 0 20px;
}
.closeicon {
    top: -3px;
    position: absolute;
    right: 0;
    width: 20px;
    height: 20px;
    border-radius: 3px;
    background-color: #d7e7f7;
    color: #666;
    text-align: center;
    line-height: 20px;
    font-size: 20px;
}
a.closeicon {
    text-decoration: none
}
.pannelHead .rtxt_title {
    display: none;
}
#cookie-notice-link {
    position: absolute;
    bottom: 0;
    left: 20px;
    z-index: 9999;
    font-family: Arial;
    font-size: 13px;
    background-color: #fff;
    padding: 2px 5px;
    border-radius: 3px;
}
#cookie-notice-link a {
    text-decoration: none
}
body {
    position: relative;
    height: 100%
}
</style>
<div class="divoverlay" id="divoverlayid">
    <div class="divsuboverlay" id="divsuboverlay_0">
        <div class="pannelHead" id="pannelHead_0">
            <div>
                <a href="#" class="closeicon" onclick="setCookie('ec_close_cookiepanel', 'true', 365);document.getElementById('divoverlayid').style.display = 'none';">×</a>
            </div>
            <div class="rtxt">
                <p class="rtxt_title"><?php echo $cookie_banner_title ?></p>
                <p><?php echo $cookie_message_line_1 ?></p>
                <p><?php echo $cookie_message_line_2 ?></p>
            </div>
        </div>
    </div>
</div>
<script>
function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) +
        ((exdays == null) ? "" : ("; expires=" + exdate.toUTCString()));
    document.cookie = c_name + "=" + c_value;
}
function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }
    return false;
}
if (getCookie('ec_close_cookiepanel') === false) {
    document.getElementById('divoverlayid').style.display = 'block';
}
</script>
<!-- Cookies top banner end -->
<div class="parsys content-area">
    <div class="title section">
        <h1 class="">
            <span>Cookie Notice</span>
        </h1>
    </div>
    <div class="footnote section">

        <p>Our website uses cookies. You can find out more about cookies, how we use them and how to control them below.</p>
        <p>By using this website you consent to the use of cookies in accordance with this Cookie Notice. If you do not consent to the use of these cookies please disable them following the instructions in this Cookie Notice so that cookies from this website cannot be placed on your device.</p>
        <h2>What is a cookie?</h2>
        <p>Cookies are small text files which are sent to your device when you visit a website. Cookies are then sent back to the originating website on each subsequent visit, or to another website that recognises that cookie. Cookies act as a memory for a website, allowing that website to remember your device on your return visits. Cookies can also remember your preferences, improve the user experience as well as tailor the adverts you see to those most relevant to you.</p>
        <p>You can find more information about cookies, including how to see what cookies have been set on your device and how to manage and delete them at <a href="http://www.aboutcookies.org" target="_blank">www.aboutcookies.org</a></p>

    </div>
    <div class="rich-text text parbase section">

        <h2>Types of cookies</h2>
        <h3>Session and persistent</h3>
        <p>We may use session cookies which exist only until you close your web browser. We may also use persistent cookies which exist for a longer, specified period of time.</p>
        <h3>Purpose</h3>
        <p>Cookies on our website are used for one or more of the below purposes.</p>
        <table width="100%" height="100%" cellspacing="0" cellpadding="1" border="1">
            <tbody>
                <tr>
                    <th>Purpose</th>
                    <th>Description</th>
                </tr>
                <tr>
                    <td>Strictly Necessary</td>
                    <td>Cookies which are strictly necessary to provide services requested by the user</td>
                </tr>
                <tr>
                    <td>Performance</td>
                    <td>Cookies which collect information about how visitors use a website, for instance which pages visitors go to most often, and if they get error messages from web pages</td>
                </tr>
                <tr>
                    <td>Functionality</td>
                    <td>Cookies which allow a website to remember choices a user makes (such as user name, language or the region) and provide enhanced, more personal features</td>
                </tr>
                <tr>
                    <td>Targeting or advertising</td>
                    <td>Cookies which are used to deliver adverts more relevant to a user and his or her interests. They are also used to limit the number of times a user sees an advertisement as well as help measure the effectiveness of the advertising campaign</td>
                </tr>
            </tbody>
        </table>
        <h3>Third party cookies</h3>
        <p>Our website may allow third party cookies to be set by services that appear on it. These cookies are not in our control. For further information about how the third party uses cookies, please visit the relevant third party website. Details of any third party cookies used on this website, including their purpose, will be outlined in the table below.</p>
        <h2>Cookies used on this website</h2>
        <p>Below is a summary of the cookies used on
            <?php echo $_SERVER['SERVER_NAME']; ?>. You should check other
                <?php echo $CLIENT; ?> websites as the use of cookies may be different.</p>
        <table width="100%" height="100%" cellspacing="0" cellpadding="1" border="1">
            <tbody>
                <tr>
                    <th>Name</th>
                    <th>Purpose</th>
                    <th>Category</th>
                    <th>Persistence</th>
                    <th>Origin</th>
                </tr>
                <tr>
                    <td>ec_close_cookiepanel</td>
                    <td>This cookie stores the information whether the user already accept the digital evidence (Cookie-notice)</td>
                    <td>Functionality</td>
                    <td>persistently</td>
                    <td>First Party</td>
                </tr>
                <tr>
                    <td>_pk_id.X.YYYY</td>
                    <td>Used by Piwik Analytics to accurately estimate the number of visitors to the website and their recurrence.</td>
                    <td width="128" valign="top">
                        <p>Performance</p>
                    </td>
                    <td width="128" valign="top">
                        <p>persistently</p>
                    </td>
                    <td width="128" valign="top">
                        <p>First Party</p>
                    </td>
                </tr>
                <tr>
                    <td>_pk_ses.X.YYYY</td>
                    <td>Used by Piwik Analytics to keep track of the tools the visitors use in each session.</td>
                    <td width="128" valign="top">
                        <p>Performance</p>
                    </td>
                    <td width="128" valign="top">
                        <p>persistently</p>
                    </td>
                    <td width="128" valign="top">
                        <p>First Party</p>
                    </td>
                </tr>
                <tr>
                    <td>_pk_ref.X.YYYY</td>
                    <td>Used by Piwik Analytics to keep track of the referrer website the visitors came from.</td>
                    <td width="128" valign="top">
                        <p>Performance</p>
                    </td>
                    <td width="128" valign="top">
                        <p>persistently</p>
                    </td>
                    <td width="128" valign="top">
                        <p>First Party</p>
                    </td>
                </tr>
            </tbody>
        </table>

    </div>
    <div class="footnote section">

        <h2>How to control and delete cookies</h2>
        <p>The majority of web browsers accept cookies, but you can usually change the browser’s settings to refuse new cookies, disable existing ones or simply let you know when new ones are sent to your device.</p>
        <p>In order to set your browser to reject cookies, refer to the help instructions supplied by the browser provider (usually located within the “Help”, “Tools” or “Edit” menu). More detailed guidance can be found at <a href="http://www.aboutcookies.org" target="_blank">www.aboutcookies.org</a></p>
        <p>Please be aware that, if you refuse or disable cookies, some of the website’s functionality may be lost. In addition, disabling a cookie or category of cookie does not delete the cookie from your browser. You will need to do this yourself from within your browser.</p>
        <h2>Changes to our use of cookies</h2>
        <p>Any changes to our use of cookies for this website will be posted here and if necessary, signposted from our web pages highlighting any changes.</p>
        <h2>Contact Information</h2>
        <p>If you have any queries in relation to this notice, please contact us at <a href="mailto:info&#64;ec-europe&#46;com">info&#64;ec-europe&#46;com</a></p>

    </div>

</div>
</body>

</html>