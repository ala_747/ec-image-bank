<?php

$json = '{
    "items" :
    [
        {
             "id": "1"
            ,"book": "Infertility"
            ,"chapter": "Infertility"
            ,"page": "101"
            ,"logofile": "almirall-inv.png"
            ,"logolink": "http://google.com"
            ,"itemtitle": "The reproductive process"
            ,"itembody": "<strong>Lorem ipsum</strong> dolor sit amet, <em>consectetur adipisicing elit</em>. Aut illum<sup>1</sup> H<sub>2</sub>O, mollitia sed repellat nulla nihil excepturi rem minus modi dicta inventore dolor ad similique fugiat sapiente quis beatae deleniti voluptate. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut illum, mollitia sed repellat nulla nihil excepturi rem minus modi dicta inventore dolor ad similique fugiat sapiente quis beatae deleniti voluptate. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut illum, mollitia sed repellat nulla nihil excepturi rem minus modi dicta inventore dolor ad similique fugiat sapiente quis beatae deleniti voluptate."
            ,"keywords": ""
            ,"score": 0
        }
        ,{
             "id": "2"
            ,"book": "Infertility"
            ,"chapter": "Infertility"
            ,"page": "102"
            ,"itemtitle": "Biological basis of reproduction"
            ,"itembody": ""
            ,"keywords": ""
            ,"score": 0
        }
        ,{
             "id": "3"
            ,"book": "Infertility"
            ,"chapter": "Infertility"
            ,"page": "103"
            ,"itemtitle": "The ovarian cycle"
            ,"itembody": ""
            ,"keywords": ""
            ,"score": 0
        }
        ,{
             "id": "4"
            ,"book": "Infertility"
            ,"chapter": "Infertility"
            ,"page": "104"
            ,"itemtitle": "Ovulation"
            ,"itembody": ""
            ,"keywords": ""
            ,"score": 0
        }
        ,{
             "id": "5"
            ,"book": "Infertility"
            ,"chapter": "Infertility"
            ,"page": "105"
            ,"itemtitle": "Causes of female infertility"
            ,"itembody": ""
            ,"keywords": ""
            ,"score": 0
        }
        ,{
             "id": "6"
            ,"book": "Infertility"
            ,"chapter": "Infertility"
            ,"page": "106"
            ,"itemtitle": "Diagnosis of female infertility"
            ,"itembody": ""
            ,"keywords": ""
            ,"score": 0
        }
        ,{
             "id": "7"
            ,"book": "Infertility"
            ,"chapter": "Infertility"
            ,"page": "107"
            ,"itemtitle": "Causes of male infertility"
            ,"itembody": ""
            ,"keywords": ""
            ,"score": 0
        }
        ,{
             "id": "8"
            ,"book": "Infertility"
            ,"chapter": "Infertility"
            ,"page": "108"
            ,"itemtitle": "Disorders of spermatogenesis: chromosomal disorders"
            ,"itembody": ""
            ,"keywords": ""
            ,"score": 0
        }
        ,{
             "id": "9"
            ,"book": "Infertility"
            ,"chapter": "Infertility"
            ,"page": "109"
            ,"itemtitle": "Assisted reproduction techniques"
            ,"itembody": ""
            ,"keywords": ""
            ,"score": 0
        }
        ,{
             "id": "10"
            ,"book": "Infertility"
            ,"chapter": "Infertility"
            ,"page": "110"
            ,"itemtitle": "Intrauterine insemination"
            ,"itembody": ""
            ,"keywords": ""
            ,"score": 0
        }
        ,{
             "id": "11"
            ,"book": "Infertility"
            ,"chapter": "Infertility"
            ,"page": "111"
            ,"itemtitle": "Oocyte retrieval"
            ,"itembody": ""
            ,"keywords": ""
            ,"score": 0
        }
        ,{
             "id": "12"
            ,"book": "Infertility"
            ,"chapter": "Infertility"
            ,"page": "112"
            ,"itemtitle": "Extracorporeal fertilization"
            ,"itembody": ""
            ,"keywords": ""
            ,"score": 0
        }
        ,{
             "id": "13"
            ,"book": "Infertility"
            ,"chapter": "Infertility"
            ,"page": "113"
            ,"itemtitle": "ICSI (intracytoplasmic sperm injection) and stages of embryonic development"
            ,"itembody": ""
            ,"keywords": ""
            ,"score": 0
        }
        ,{
             "id": "14"
            ,"book": "Infertility"
            ,"chapter": "Infertility"
            ,"page": "114"
            ,"itemtitle": "Pre-implantation genetic diagnosis"
            ,"itembody": ""
            ,"keywords": ""
            ,"score": 0
        }
        ,{
             "id": "15"
            ,"book": "Infertility"
            ,"chapter": "Infertility"
            ,"page": "115"
            ,"itemtitle": "ÓFrozen embryo transfer"
            ,"itembody": ""
            ,"keywords": ""
            ,"score": 0
        }
    ]
}';

// header not needed since we use require to insert json into index
// would only be needed if called using ajax
// header('Content-Type: application/json');
echo $json;

?>