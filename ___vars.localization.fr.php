<?php

// ------------------------------------------------------- toplinks / search
$home    = "Accueil";
$help    = "Bibliographie";
$tos     = "Conditions d’utilisation";
$cookies = "Cookies";
$search  = "Rechercher";
$ready   = "Cliquer sur une image pour ouvrir l’éditeur";
$filtrby = "";
$fulltext= "";
// ------------------------------------------------------- login
$strLogin= "Login";
$strUser = "Identifiant";
$strPass = "Mot de passe";
$strError= "Identifiant ou mot de passe non valide";
// ------------------------------------------------------- actions
$Text    = "Texte";
$addtxt  = "Ajouter une ligne de texte";
$addok   = "(modifiable ultérieurement)";
$Line    = "Ligne";
$Arrow   = "Flèche";
$Pencil  = "Crayon";
$Crop    = "Rogner";
$Rect    = "Rectangle";
$Circle  = "Cercle";
$Filter  = "Filtres";
$Undo    = "Annuler";
$Redo    = "Répéter";
$Save    = "Sauvegarder";
$Print   = "Imprimer";
$FS      = "Taille de la police";
$Color   = "Couleur";
$LW      = "Largeur de la ligne";
$SW      = "Largeur de la bordure";
$SC      = "Couleur de la bordure";
$FC      = "Couleur de remplissage";
$BR      = "Luminosité";
$CN      = "Contraste";
$BW      = "Noir et blanc";
// ------------------------------------------------------- undo/redo, color pickers
$init    = "new canvas";
$new     = "objet inséré: ";
$mod     = "objet modifié: ";
$del     = "objet supprimé: ";
$prop    = "feature modified: ";
$cancel  = "annuler";
$ok      = "OK";
// ------------------------------------------------------- cookie banner (remember to escape eventual ' chars like \' into messages)
$cookie_banner_title = 'Cookies Information';
$cookie_message_line_1 = 'Ce site utilise des cookies pour nous aider à vous donner la meilleure expérience lorsque vous visitez notre site Web. En continuant à utiliser ce site, vous consentez à notre utilisation de ces cookies.';
$cookie_message_line_2 = 'Pour en savoir plus sur la façon dont nous utilisons les cookies et comment les gérer en lisant notre <a class="cookie-notice" href="cookie-notice.'. $LANG .'.php" target="cookie_notice">cookies préavis</a>.';
